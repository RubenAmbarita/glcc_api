<?php

use Phalcon\Mvc\Router;

	$router = new Router();

    //Remove trailing slashes automatically
    $router->removeExtraSlashes(true);

    $router->add("/Auth/login", "Auth::login");
		$router->add("/Auth/register", "Auth::register");
		$router->add("/otp/confirmOtp", "Otp::confirmOtp");
		$router->add("/partner/jenisPartner", "Partner::jenisPartner");
		$router->add("/vehicle/insertVehicle", "Vehicle::insertVehicle");
		$router->add("/Search/searchPosition", "Search::search");
		$router->add("/Save/saveData","Save::save");
		$router->add("/dashboard/dashboardAgent","DashboardAgent::dashboard");
		$router->add("/orderAgen/orderAgenMonth","OrderAgentMonth::order");
		$router->add("/orderAgen/orderAgenDay","OrderAgentDay::order");
		$router->add("/orderAgen/orderAgenNonProses","OrderAgentNonProses::order");
		$router->add("/orderAccept/orderAcceptAgent","OrderAcceptAgent::OrderAccept");
		$router->add("/orderAccept/updateOrderAgent","OrderAcceptAgent::updateOrderStatus");
		$router->add("/info/infoForm","InfoForm::info");

		$router->add("/Bokspoint/getBokspoint","OrderInBokspoint::getBokspoint");

		$router->add("/savefoto/savefoto","Savefoto::savefoto");
		$router->add("/savefoto/savefotoBarang","SaveTerimaBarang::savefoto");
    $router->add("/Order/Neworder", "Order::Neworder");
    $router->add("/Order/update", "Order::update");
    $router->add("/Order/upload", "Order::upload");
    $router->add("/Master/PickupLoc", "Master::PickupLoc");

return $router;
