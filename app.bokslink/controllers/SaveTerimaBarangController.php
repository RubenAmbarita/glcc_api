<?php

/**
 *
 */
class SaveTerimaBarangController extends ControllerBase
{

  public function savefotoAction()
  {
    // code...
    $SafefotoModel = new saveFotoTerimaBarangModel();
    $percent = 0.5;
    $fileNewName = time();
    $folderPath = "/xampp/htdocs/api/fotoBokspartner/";
    $param['nama_pengirim'] = $this->request->getPost('nama_pengirim');
    $param['order_id'] = $this->request->getPost('order_id');

    if(isset($_FILES['pickup_assignment_photo']['tmp_name'])){
			$file = $_FILES['pickup_assignment_photo']['tmp_name'];
			$ext = pathinfo($_FILES['pickup_assignment_photo']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['pickup_assignment_photo']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "tanda_tangan". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "tanda_tangan". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$param['pickup_assignment_photo'] = $data.$ext;
		}

    if(isset($_FILES['shipper_goods_photo']['tmp_name'])){
			$file = $_FILES['shipper_goods_photo']['tmp_name'];
			$ext = pathinfo($_FILES['shipper_goods_photo']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['shipper_goods_photo']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "foto_barang". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "foto_barang". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$param['shipper_goods_photo'] = $data.$ext;
		}

    if(!empty($param))
    {
        $result = $SafefotoModel->saveFoto($param);

        if($result['status']==true){
            $result['message'] = 'sukses';
            $results = $this->get_results($result);
        }else{
          $result['message'] = 'failed';
          $results = $this->get_results($result);
        }
        return json_encode($results, JSON_PRETTY_PRINT);
    }
  }

  public function imageResize($imageResourceId,$width,$height)
  {
      $targetWidth = 500;
      $diff = $width / $targetWidth;
      $targetHeight = $height / $diff;

      $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
      return $targetLayer;
  }
}


 ?>
