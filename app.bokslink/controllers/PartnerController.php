<?php


/**
 *
 */
class PartnerController extends ControllerBase
{

  public function jenisPartnerAction()
  {
    // code...
    $PartnerModel = new PartnerModel();

    $param['email'] = $this->request->getPost('email');
    $param['is_courier'] = $this->request->getPost('is_courier');
    $param['is_trip'] = $this->request->getPost('is_trip');
    $param['is_agent'] = $this->request->getPost('is_agent');


    if(!empty($param))
    {
        $result = $PartnerModel->partner($param);
        $result['is_courier'] = $param['is_courier'];
        $result['is_trip'] = $param['is_trip'];
        $result['is_agent'] = $param['is_agent'];
        $result = $this->get_results($result);
    }else{
      $result = '';
      $result['is_courier'] = '';
      $result['is_trip'] = '';
      $result['is_agent'] = '';
      $result = $this->get_results($result);
    }

    return json_encode($result, JSON_PRETTY_PRINT);
  }
}

 ?>
