<?php

/**
 *
 */
class SearchController extends ControllerBase
{

  public function searchAction()
  {
    $searchModel = new SearchModel();
    $param = $this->request->getPost('address');

    // print_r($param);
    if(!empty($param)){

      // $result = $searchModel->getLocation($param);
      $result = $searchModel->getLocation($param);

      return json_encode($result,JSON_PRETTY_PRINT);
}

}
}
