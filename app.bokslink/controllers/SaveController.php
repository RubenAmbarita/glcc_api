<?php


/**
 *
 */
class SaveController extends ControllerBase
{

  public function saveAction()
  {
      $SaveModel = new SaveModel();
      $percent = 0.5;
      $fileNewName = time();
      $folderPath = "/xampp/htdocs/api/fotoBokspartner/";
  $p_param['id'] = $this->request->getPost('id');
  $p_param['email'] = $this->request->getPost('email');
  $p_param['alamat'] = $this->request->getPost('alamat');
  $p_param['gedung'] = $this->request->getPost('gedung');
  $p_param['provinsi'] = $this->request->getPost('provinsi');
  $p_param['kota'] = $this->request->getPost('kota');
  $p_param['kecamatan'] = $this->request->getPost('kecamatan');
  $p_param['kelurahan'] = $this->request->getPost('kelurahan');
  $p_param['kodepos'] = $this->request->getPost('kodepos');

  if(isset($_FILES['lokasi1']['tmp_name'])){
    $file = $_FILES['lokasi1']['tmp_name'];
    $ext = pathinfo($_FILES['lokasi1']['name'], PATHINFO_EXTENSION);
    $filename = basename( $_FILES['lokasi1']['name']);
    $sourceProperties = getimagesize($file);
    $targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
    $imageType = $sourceProperties[2];
    if($imageType==IMAGETYPE_JPEG){
      $imageResourceId = imagecreatefromjpeg($file);
      imagejpeg($imageResourceId,$folderPath. "lokasi1". $fileNewName."_thump.".$ext);
    }else{
      $imageResourceId = imagecreatefrompng($file);
      imagepng($imageResourceId,$folderPath. "lokasi1". $fileNewName. "_thump.". $ext);
    }
    $data = base64_encode($filename);
    $p_param['lokasi1'] = $data.$ext;
  }

  if(isset($_FILES['lokasi2']['tmp_name'])){
    $file = $_FILES['lokasi2']['tmp_name'];
    $ext = pathinfo($_FILES['lokasi2']['name'], PATHINFO_EXTENSION);
    $filename = basename( $_FILES['lokasi2']['name']);
    $sourceProperties = getimagesize($file);
    $targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
    $imageType = $sourceProperties[2];
    if($imageType==IMAGETYPE_JPEG){
      $imageResourceId = imagecreatefromjpeg($file);
      imagejpeg($imageResourceId,$folderPath. "lokasi2". $fileNewName."_thump.".$ext);
    }else{
      $imageResourceId = imagecreatefrompng($file);
      imagepng($imageResourceId,$folderPath. "lokasi2". $fileNewName. "_thump.". $ext);
    }
    $data = base64_encode($filename);
    $p_param['lokasi2'] = $data.$ext;
  }

  $p_param['panjang'] = $this->request->getPost('panjang');
  $p_param['lebar'] = $this->request->getPost('lebar');
  $p_param['tinggi'] = $this->request->getPost('tinggi');
  $p_param['is_table'] = $this->request->getPost('is_table');
  $p_param['is_chair'] = $this->request->getPost('is_chair');
  $p_param['is_ac'] = $this->request->getPost('is_ac');
  $p_param['is_fan'] = $this->request->getPost('is_fan');
  $p_param['is_room'] = $this->request->getPost('is_room');

  if(!empty($p_param))
  {
      $result = $SaveModel->saveData($p_param);

      if($result['status']==true){
          $result['message'] = 'sukses';
          $results = $this->get_results($result);
      }else{
        $result['message'] = 'failed';
        $results = $this->get_results($result);
      }
      return json_encode($results, JSON_PRETTY_PRINT);
  }
  }

  public function imageResize($imageResourceId,$width,$height)
  {
      $targetWidth = 500;
      $diff = $width / $targetWidth;
      $targetHeight = $height / $diff;

      $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
      return $targetLayer;
  }
}
 ?>
