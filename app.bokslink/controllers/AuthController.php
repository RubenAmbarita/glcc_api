<?php
//require('library/email/class.phpmailer.php');
//require('library/email/class.smtp.php');
use \PHPMailer as PHPMailer;
date_default_timezone_set('Asia/Jakarta');

class AuthController extends ControllerBase
{

  public function loginAction()
  {
      $Auth = new Auth();

      $p_param['email'] = $this->request->getPost('email');
      $p_param['password'] = md5($this->request->getPost('password'));
      //$p_param['activity_token'] = $this->request->getPost('u_token')??$this->getToken(34);
      $p_param['activity_token'] = $this->getToken(30);
      //$p_param['driver_image'] = $this->driver_image;
      //$p_param['upload_image'] = $this->upload_image;

      if(!empty($p_param))
      {
          $result = $Auth->get_login($p_param);

          if($result['status']==true)
          {
              $result['activity_token'] = $p_param['activity_token'];
              $update = $Auth->update_tokenPartner($result['activity_token'], $p_param['email']);
          }
          else{
              $result['activity_token'] = '';
          }

          $result = $this->get_results($result);
      }
      else
      {
          $result = '';
          $result = $this->get_results($result);
      }

      return json_encode($result, JSON_PRETTY_PRINT);
  }

    public function registerAction()
    {
        $Auth = new Auth();

        $p_param['name'] = $this->request->getPost('name');
        $p_param['adress'] = $this->request->getPost('adress');
        $p_param['username'] = $this->request->getPost('username');
        $p_param['email'] = $this->request->getPost('email');
        $p_param['password'] = md5($this->request->getPost('password'));
        $p_param['sysdate'] = date('Y-m-d H:i:s');
        $p_param['otp'] = $this->getOTP(6);
        $p_param['activity_token'] = $this->getToken(30);

        if(!empty($p_param))
        {
            $result = $Auth->get_registered($p_param);

            if($result['status']==true)
            {
                $result['activity_token'] = $p_param['activity_token'];
                $update = $Auth->update_tokenPartner($result['activity_token'], $p_param['email']);
                $this->sendEmailPartner($p_param['otp'],$p_param['email'],$p_param['password']);
            }
            else{
                $result['otp'] = '';
            }

            $result = $this->get_results($result);
        }
        else
        {
            // $result['status'] = false;
            // $result['StatusReg'] = 'repassword wrong';
            // $result = $this->get_results($result);
            $status = false;
            $data['statusReg'] = 'rePassword wrong';
            $StatusReg = $data;
            $result = array('status' => $status, 'StatusReg' => $StatusReg);
            $result['activity_token'] = '';
            $result = $this->get_results($result);
        }

        return json_encode($result, JSON_PRETTY_PRINT);
    }

  public function sendEmailPartner($OTP,$email,$password)
{
      // Create the Transport
  $transport = (new Swift_SmtpTransport('smtp.gmail.com', 587,'tls'))
    ->setUsername('rubenambarita7@gmail.com')
    ->setPassword('Ruben0622434307');

  // Create the Mailer using your created Transport
  $mailer = new Swift_Mailer($transport);

  // Create a message
  $message = (new Swift_Message($OTP))
    ->setFrom(['angelboksman@gmail.com' => 'BoksLink'])
    ->setTo([$email, 'angelboksman@gmail.com' => 'Company'])
    ->setBody('Berikut No OTP Anda'.$OTP.' Harap Segera Konfirmasi!')
    ;

  // Send the message
  $result = $mailer->send($message);
}
}
