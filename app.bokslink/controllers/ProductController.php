<?php

class ProductController extends ControllerBase
{
    public function listAction()
    {
        $Product = new Product();

        $result = $Product->get_product($p_param);
        $result = $this->get_results($result);

        return json_encode($result, JSON_PRETTY_PRINT);
    }

}

