<?php

  /**
   *
   */
  class VehicleController extends ControllerBase
  {

    public function insertVehicleAction()
    {
      // code...
      $VehicleModel = new VehicleModel();
      $param['email'] = $this->request->getPost('email');
      $param['vehicle_type'] = $this->request->getPost('vehicle_type');
      $param['vehicle_year'] = $this->request->getPost('vehicle_year');
      $param['vehicle_brand'] = $this->request->getPost('vehicle_brand');

      if(!empty($param)){
        $data = $VehicleModel->vehicle($param);

        if($data['status']==true){
          $data['email'] = $param['email'];
          $data['vehicle_type'] = $param['vehicle_type'];
          $data['vehicle_year'] = $param['vehicle_year'];
          $data['vehicle_brand'] = $param['vehicle_brand'];
          $data = $this->get_results($data);
        }else{
        $data['email'] = '';
        $data['vehicle_type'] = '';
        $data['vehicle_year'] = '';
        $data['vehicle_brand'] = '';
        $data = $this->get_results($data);
      }

      return json_encode($data, JSON_PRETTY_PRINT);
    }
  }
}

 ?>
