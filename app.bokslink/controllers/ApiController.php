<?php

class ApiController extends ControllerBase
{
    public function loginAction()
    {
        $Api = new Api();

        $p_param['username'] = $this->request->getPost('username');
        $p_param['password'] = md5($this->request->getPost('password') . $this->SecretKey);
        //$p_param['activity_token'] = $this->request->getPost('u_token')??$this->getToken(34);
        $p_param['activity_token'] = $this->getToken(34);

        if(!empty($p_param))
        {
            $result = $Api->get_auth($p_param);

            if($result['status']=true)
            {
                $result['activity_token'] = $p_param['activity_token'];
                $update = $Api->update_token($result['activity_token'], $p_param['username']);
            }
            else{
                $result['activity_token'] = '';
            }

            $result = $this->get_results($result);
        }
        else
        {
            $result = '';
            $result = $this->get_results($result);
        }

        return json_encode($result, JSON_PRETTY_PRINT);
    }

    public function NeworderAction()
    {
        $Api = new Api();

        $p_param['driverid'] = $this->request->getPost('driver_id');
        $p_param['token'] = $this->request->getPost('token');

        if(!empty($p_param))
        {
            $result = $Api->get_neworder($p_param);
            if($result['status']=true)
            {
                $result['activity_token'] = $this->getToken(34);
                $update = $Api->newupdate_token($p_param['token'], $result['activity_token'], $p_param['driverid']);
            }
            else{
                $result['activity_token'] = '';
            }

            $result = $this->get_results($result);
        }
        else
        {
            $result = '';
            $result = $this->get_results($result);
        }

        return json_encode($result, JSON_PRETTY_PRINT);
    }

    public function updateAction()
    {
        $Api = new Api();

        $p_param['username'] = $this->request->getPost('username');
        $p_param['token'] = $this->request->getPost('token');
        $p_param['status'] = $this->request->getPost('status');

        if(!empty($p_param))
        {
            $p_param['activity_token'] = $this->getToken(34);
            $result = $Api->newupdate_status($p_param);
            $result = $this->get_GlobalResult($result, $p_param['activity_token']);
        }
        else
        {
            $result = $this->get_GlobalResult(false, '');
        }

        return json_encode($result, JSON_PRETTY_PRINT);
    }
}

