<?php

class MasterController extends ControllerBase
{
    public function PickupLocListAction()
    {
        $Master = new Master();

        $p_param['shipperid'] = $this->request->getPost('shipperid');
        //$p_param['address'] = 'Raya';

        if(!empty($p_param))
        {
            $result = $Master->get_locationlist($p_param);

            if(!empty($result['status']))
            {
                $result['activity_token'] = $this->getToken(34);
                //$update = $Order->update_token($p_param['token'], $result['activity_token'], $p_param['driverid']);
            }
            else{
                $result['activity_token'] = '';
            }

            $result = $this->get_results($result);
        }
        else
        {
            $result = '';
            $result = $this->get_results($result);
        }
        print_r($result);
        //return json_encode($result, JSON_PRETTY_PRINT);
    }

    public function PickupLocAction()
    {
        $Master = new Master();

        $p_param['address'] = $this->request->getPost('address');
        //$p_param['address'] = 'Raya';

        if(!empty($p_param))
        {
            $result = $Master->get_address($p_param);

            if(!empty($result['status']))
            {
                $result['activity_token'] = $this->getToken(34);
                //$update = $Order->update_token($p_param['token'], $result['activity_token'], $p_param['driverid']);
            }
            else{
                $result['activity_token'] = '';
            }

            //$result = $this->get_results($result);
        }
        else
        {
            $result = '';
            //$result = $this->get_results($result);
        }
        print_r($result);
        //return json_encode($result, JSON_PRETTY_PRINT);
    }

    public function updateAction()
    {
        $Order = new Order();

        $p_param['orderdetailid'] = $this->request->getPost('orderdetailid');
        $p_param['orderid'] = $this->request->getPost('orderid');
        $p_param['username'] = $this->request->getPost('username');
        $p_param['driverid'] = $this->request->getPost('driverid');
        $p_param['token'] = $this->request->getPost('token');
        $p_param['status'] = $this->request->getPost('status');
        $p_param['imageupload'] = $this->request->getPost('imagename');

        if(!empty($p_param))
        {
            $p_param['activity_token'] = $this->getToken(34);
            $result = $Order->update_status($p_param);
            $result = $this->get_GlobalResult($result, $p_param['activity_token']);
        }
        else
        {
            $result = $this->get_GlobalResult(false, '');
        }

        return json_encode($result, JSON_PRETTY_PRINT);
    }

    public function uploadxAction()
    {
        $file_path = "/var/www/html/bokscube.trucker/assets/upload/";
        $file_path = $file_path . basename( $_FILES['uploaded_file']['name']);
        if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $file_path) ){
            echo "success";
        } else{
            echo "fail";
        }
    }

    public function uploadAction()
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = "/usr/share/nginx/html/bokscube.trucker/assets/upload/";
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "wh_". $fileNewName. "_thump.". $ext);
                    $return['status'] = true;
                    $return['name'] = "wh_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "wh_". $fileNewName. "_thump.". $ext);
                    $return['status'] = true;
                    $return['name'] = "wh_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "wh_". $fileNewName. "_thump.". $ext);
                    $return['status'] = true;
                    $return['name'] = "wh_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return['status'] = false;
                    $return['name'] = "";
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            $result = $this->get_results($return);

            return json_encode($result, JSON_PRETTY_PRINT);
        }
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;
    
        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;
        
        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

}

