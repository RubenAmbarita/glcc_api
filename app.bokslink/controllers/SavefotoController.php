<?php

class SavefotoController extends ControllerBase
{
	public function savefotoAction()
    {
        $SafefotoModel = new SafefotoModel();
				$percent = 0.5;
				$fileNewName = time();
				$folderPath = "/xampp/htdocs/api/fotoBokspartner/";

    $p_param['email'] = $this->request->getPost('email');
		$p_param['ktp_num'] = $this->request->getPost('ktp_num');

		if(isset($_FILES['ktp_image']['tmp_name'])){
			$file = $_FILES['ktp_image']['tmp_name'];
			$ext = pathinfo($_FILES['ktp_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['ktp_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_ktp". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_ktp". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['ktp_image'] = $data.$ext;
		}

		$p_param['kk_num'] = $this->request->getPost('kk_num');

		if(isset($_FILES['kk_image']['tmp_name'])){
			$file = $_FILES['kk_image']['tmp_name'];
			$ext = pathinfo($_FILES['kk_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['kk_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_kk". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_kk". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['kk_image'] = $data.$ext;
		}

		$p_param['skck_num'] = $this->request->getPost('skck_num');

		if(isset($_FILES['skck_image']['tmp_name'])){
			$file = $_FILES['skck_image']['tmp_name'];
			$ext = pathinfo($_FILES['skck_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['skck_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_skck". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_skck". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['skck_image'] = $data.$ext;
		}

		$p_param['sim_num'] = $this->request->getPost('sim_num');

		if(isset($_FILES['sim_image']['tmp_name'])){
			$file = $_FILES['sim_image']['tmp_name'];
			$ext = pathinfo($_FILES['sim_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['sim_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_sim". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_sim". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['sim_image'] = $data.$ext;
		}

		$p_param['stnk_num'] = $this->request->getPost('stnk_num');

		if(isset($_FILES['stnk_image']['tmp_name'])){
			$file = $_FILES['stnk_image']['tmp_name'];
			$ext = pathinfo($_FILES['stnk_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['stnk_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_stnk". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_stnk". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['stnk_image'] = $data.$ext;
		}

		if(isset($_FILES['fotodiri_image']['tmp_name'])){
			$file = $_FILES['fotodiri_image']['tmp_name'];
			$ext = pathinfo($_FILES['fotodiri_image']['name'], PATHINFO_EXTENSION);
			$filename = basename( $_FILES['fotodiri_image']['name']);
			$sourceProperties = getimagesize($file);
			$targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
			$imageType = $sourceProperties[2];
			if($imageType==IMAGETYPE_JPEG){
				$imageResourceId = imagecreatefromjpeg($file);
				imagejpeg($imageResourceId,$folderPath. "boks_partner_fotodiri". $fileNewName."_thump.".$ext);
			}else{
				$imageResourceId = imagecreatefrompng($file);
				imagepng($imageResourceId,$folderPath. "boks_partner_fotodiri". $fileNewName. "_thump.". $ext);
			}
			$data = base64_encode($filename);
			$p_param['fotodiri_image'] = $data.$ext;
		}

        if(!empty($p_param))
        {
            $result = $SafefotoModel->insert_partner_identity($p_param);

						if($result['status']==true){
								$result['message'] = 'sukses';
								$results = $this->get_results($result);
						}else{
							$result['message'] = 'failed';
							$results = $this->get_results($result);
						}
						return json_encode($results, JSON_PRETTY_PRINT);
        }

    }

		public function imageResize($imageResourceId,$width,$height)
    {
        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        return $targetLayer;
    }
}
