<?php
date_default_timezone_set('Asia/Jakarta');
/**
 *
 */
class OrderAcceptAgentController extends ControllerBase
{

  public function OrderAcceptAction()
  {
    $param['order_id'] = $this->request->getPost('order_id');
    $param['partner_id'] = $this->request->getPost('partner_id');
    $param['status'] = $this->request->getPost('status');
    $param['sysdate'] = date('Y-m-d H:i:s');
    $OrderAccept = new OrderAcceptAgentModel();

    if(!empty($param)){
      $result = $OrderAccept->getOrderAccept($param);
      $result = $this->get_results($result);
    }else {
      // code...
      $result = '';
      $result = $this->get_results($result);
    }
    return json_encode($result, JSON_PRETTY_PRINT);
  }

  public function updateOrderStatusAction()
  {
    $param['order_status'] = $this->request->getPost('order_status');
    $param['order_id'] = $this->request->getPost('order_id');
    $updateStatusOrder = new OrderAcceptAgentModel();

    if(!empty($param))
    {
      $result = $updateStatusOrder->updateStatusOrder($param);
      $result = $this->get_results($result);
;    }else {
      // code...
      $result = '';
      $result = $this->get_results($result);
    }
    return json_encode($result, JSON_PRETTY_PRINT);
  }
}

 ?>
