<?php

/**
 *
 */
class DashboardAgentController extends ControllerBase
{

  public function dashboardAction()
  {
    // code...
    $dashboardAgent = new DashboardAgentModel();
    $param['id'] = $this->request->getPost('id');

    if(!empty($param)){

      $results = $dashboardAgent->getDashboard($param);
      return json_encode($results,JSON_PRETTY_PRINT);
      }else{
        return json_encode($results,JSON_PRETTY_PRINT);
      }
    }
  }

 ?>
