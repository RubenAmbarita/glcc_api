<?php

use Phalcon\Mvc\Controller;

require_once '../vendor/autoload.php';

class ControllerBase extends Controller
{

    function getToken($length)
    {
        $token = "";
        $codeAlphabet = "BoksMan.Com";
        $codeAlphabet.= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }

    function getOTP($length)
    {
        $otp = "";
        $codeNumeric= "0123456789";
        $max = strlen($codeNumeric); // edited

        for ($i=0; $i < $length; $i++) {
            $otp .= $codeNumeric[random_int(0, $max-1)];
        }

        return $otp;
    }

    public function get_results($p_result)
    {
        if ($p_result['status']==false)
        {
            $result = array('status' => 0, 'message' => 'Failed', 'data' => array($p_result));
        }
        else{
            $result = array('status' => 1, 'message' => 'Succeeded', 'data' => array($p_result));
        }

        return $result;
    }

    public function get_results_2($p_result)
    {
        if ($p_result<0)
        {
            $result =$p_result;
        }
        else{
            $result = $p_result;
        }

        return $result;
    }
}
