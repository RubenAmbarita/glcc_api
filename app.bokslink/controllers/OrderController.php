<?php

class OrderController extends ControllerBase
{

  //fungsi simpan detail order
  public function simpanLokasiAction(){
    $SearchModel = new Order();

    $p_param['order_code'] = $this->getOTP(6);
    $p_param['email'] = $this->request->getPost('email');
    $p_param['service_type'] = $this->request->getPost('service_type'); //Intracity atau Intercity
    $p_param['subservice_type'] = $this->request->getPost('subservice_type'); //3, 12, 24 atau 24, 72

    $p_param['lokasi_pickup'] = $this->request->getPost('lokasi_pickup');
    $p_param['nama_pengirim'] = $this->request->getPost('nama_pengirim');
    $p_param['mp_pengirim'] = $this->request->getPost('mp_pengirim');

    $p_param['lokasi_drop'] = $this->request->getPost('lokasi_drop');
    $p_param['nama_penerima'] = $this->request->getPost('nama_penerima');
    $p_param['mp_penerima'] = $this->request->getPost('mp_penerima');

    $p_param['goods_category'] = $this->request->getPost('goods_category');
    $p_param['goods_subcategory'] = $this->request->getPost('goods_subcategory');
    $p_param['goods_dimension_width'] = $this->request->getPost('goods_dimension_width');
    $p_param['goods_dimension_length'] = $this->request->getPost('goods_dimension_length');
    $p_param['goods_dimension_height'] = $this->request->getPost('goods_dimension_height');
    $p_param['goods_dimension_equiweight'] = $this->request->getPost('goods_dimension_equiweight');
    $p_param['goods_dimension_weight'] = $this->request->getPost('goods_dimension_weight');
    $p_param['goods_quantity'] = $this->request->getPost('goods_quantity');
    $p_param['goods_value'] = $this->request->getPost('goods_value');
    $p_param['imageupload'] = $this->request->getPost('imageupload');
    $p_param['description'] = $this->request->getPost('description');

    $p_param['is_insured'] = $this->request->getPost('is_insured');
    $p_param['insurance_amount'] = $this->request->getPost('insurance_amount');
    $p_param['charge_amount'] = $this->request->getPost('charge_amount');
    $p_param['kodePromo'] = $this->request->getPost('kodePromo');
    $p_param['payment_method'] = $this->request->getPost('payment_method');

    $p_param['activity_token'] = $this->getToken(34);

    if(!empty($p_param))
    {
      if(isset($_FILES['imageupload']['tmp_name'])){

        // $order_img = base64_encode(file_get_contents($_FILES['imageupload']['tmp_name']));
        // $p_param['imageupload'] = $order_img;

        $percent = 0.5;
        $fileNewName = time();
        $folderPath = "/xampp/htdocs/api/fotoDetailBarang/";

        $file = $_FILES['imageupload']['tmp_name'];
        $ext = pathinfo($_FILES['imageupload']['name'], PATHINFO_EXTENSION);
        $filename = basename( $_FILES['imageupload']['name']);

        $sourceProperties = getimagesize($file);
        $targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
        $imageType = $sourceProperties[2];

        if($imageType==IMAGETYPE_JPEG){
          $imageResourceId = imagecreatefromjpeg($file);
          imagejpeg($imageResourceId,$folderPath. "Detail_Barang". $fileNewName."_thump.".$ext);
        }else{
          $imageResourceId = imagecreatefrompng($file);
          imagepng($imageResourceId,$folderPath. "Detail_Barang". $fileNewName. "_thump.". $ext);
        }
        $data = base64_encode($filename);
        $p_param['imageupload'] = $data.$ext;

        $shipperId = $SearchModel->cariShipperID($p_param);

        $result1 = $SearchModel->cariDetaiLokasiPickup($p_param, $shipperId);
        $result2 = $SearchModel->cariDetaiLokasiDrop($p_param, $shipperId);

        if($result1 > 0 && $result2 > 0)
        {
          $dataLokasi['lokasi_id_pickup'] = $result1['lokasi_boks_building_id'];//building_pickup_id
          $dataLokasi['lokasi_nama_pickup'] = $result1['lokasi_nama']; //label_pickup
          $dataLokasi['lokasi_address_1_pickup'] = $result1['lokasi_address_1']; //address1_pickup
          $dataLokasi['lokasi_address_2_pickup'] = $result1['lokasi_address_2']; //address2_pickup
          $dataLokasi['lokasi_prov_code_pickup'] = $result1['lokasi_prov_code']; //prov_code_pickup
          $dataLokasi['lokasi_kokab_code_pickup'] = $result1['lokasi_kokab_code']; //kokab_code_pickup
          $dataLokasi['lokasi_kec_code_pickup'] = $result1['lokasi_kec_code']; //kec_code_pickup
          $dataLokasi['lokasi_kel_code_pickup'] = $result1['lokasi_kel_code']; //kec_code_pickup
          $dataLokasi['lokasi_rt_pickup'] = $result1['lokasi_rt']; //rt_pickup
          $dataLokasi['lokasi_rw_pickup'] = $result1['lokasi_rw']; //rw_pickup
          $dataLokasi['lokasi_post_code_pickup'] = $result1['lokasi_post_code']; //post_code_pickup
          $dataLokasi['lokasi_longitude_pickup'] = $result1['lokasi_longitude']; //logitude_pickup
          $dataLokasi['lokasi_latitude_pickup'] = $result1['lokasi_latitude']; //latitude_pickup
          $dataLokasi['lokasi_id_drop'] = $result2['lokasi_boks_building_id'];//building_drop_id
          $dataLokasi['lokasi_nama_drop'] = $result2['lokasi_nama']; //label_drop
          $dataLokasi['lokasi_address_1_drop'] = $result2['lokasi_address_1']; //address1_drop
          $dataLokasi['lokasi_address_2_drop'] = $result2['lokasi_address_2']; //address2_drop
          $dataLokasi['lokasi_prov_code_drop'] = $result2['lokasi_prov_code']; //prov_code_drop
          $dataLokasi['lokasi_kokab_code_drop'] = $result2['lokasi_kokab_code']; //kokab_code_drop
          $dataLokasi['lokasi_kec_code_drop'] = $result2['lokasi_kec_code']; //kec_code_drop
          $dataLokasi['lokasi_kel_code_drop'] = $result2['lokasi_kel_code']; //kec_code_drop
          $dataLokasi['lokasi_rt_drop'] = $result2['lokasi_rt']; //rt_drop
          $dataLokasi['lokasi_rw_drop'] = $result2['lokasi_rw']; //rw_drop
          $dataLokasi['lokasi_post_code_drop'] = $result2['lokasi_post_code']; //post_code_drop
          $dataLokasi['lokasi_longitude_drop'] = $result2['lokasi_longitude']; //logitude_drop
          $dataLokasi['lokasi_latitude_drop'] = $result2['lokasi_latitude']; //latitude_drop
          $dataOrder = $dataLokasi;
          $simpanOrder = $SearchModel->simpanDetailOrder($dataOrder, $p_param, $shipperId);
          // $simpanOrderTrack = $SearchModel->cariOrder($p_param);
        }
        else{
          $dataLokasi['lokasi_id_pickup'] = '';//building_pickup_id
          $dataLokasi['lokasi_nama_pickup'] = ''; //label_pickup
          $dataLokasi['lokasi_address_1_pickup'] = ''; //address1_pickup
          $dataLokasi['lokasi_address_2_pickup'] = ''; //address2_pickup
          $dataLokasi['lokasi_prov_code_pickup'] = ''; //prov_code_pickup
          $dataLokasi['lokasi_kokab_code_pickup'] = ''; //kokab_code_pickup
          $dataLokasi['lokasi_kec_code_pickup'] = ''; //kec_code_pickup
          $dataLokasi['lokasi_kel_code_pickup'] = ''; //kec_code_pickup
          $dataLokasi['lokasi_rt_pickup'] = ''; //rt_pickup
          $dataLokasi['lokasi_rw_pickup'] = ''; //rw_pickup
          $dataLokasi['lokasi_post_code_pickup'] = ''; //post_code_pickup
          $dataLokasi['lokasi_longitude_pickup'] = ''; //logitude_pickup
          $dataLokasi['lokasi_latitude_pickup'] = '';//latitude_pickup
          $dataLokasi['lokasi_id_drop'] = '';//building_drop_id
          $dataLokasi['lokasi_nama_drop'] = ''; //label_drop
          $dataLokasi['lokasi_address_1_drop'] = ''; //address1_drop
          $dataLokasi['lokasi_address_2_drop'] = ''; //address2_drop
          $dataLokasi['lokasi_prov_code_drop'] =''; //prov_code_drop
          $dataLokasi['lokasi_kokab_code_drop'] = ''; //kokab_code_drop
          $dataLokasi['lokasi_kec_code_drop'] = ''; //kec_code_drop
          $dataLokasi['lokasi_kel_code_drop'] =''; //kec_code_drop
          $dataLokasi['lokasi_rt_drop'] = ''; //rt_drop
          $dataLokasi['lokasi_rw_drop'] = ''; //rw_drop
          $dataLokasi['lokasi_post_code_drop'] = ''; //post_code_drop
          $dataLokasi['lokasi_longitude_drop'] = ''; //logitude_drop
          $dataLokasi['lokasi_latitude_drop'] = ''; //latitude_drop

          $dataOrder = $dataLokasi;

          $simpanOrder = $SearchModel->simpanDetailOrder($dataOrder, $p_param);
          // $simpanOrderTrack = $SearchModel->cariDetailOrderTrack($p_param);
          // $cariOrder = $SearchModel->cariOrder($p_param);
        }

        $result = $simpanOrder;
      }
      else{
        $result = ' Tidak ada data';
      }
    }

    else
    {
      $result = '';
    }

    $result = $this->get_results_2($result);

    return json_encode($result, JSON_PRETTY_PRINT);
  }


public function cariDetailOrderTrackAction(){
  $p_param['order_code'] = $this->request->getPost('order_code');

  if(!empty($p_param)){
    $Order = new Order();
    $result = $Order->cariDetailOrderTrack($p_param);
  }
  else{
    $result = '';
  }

  $result = $this->get_results_2($result);

  return json_encode($result, JSON_PRETTY_PRINT);
}

public function cariShipperIDAction(){
  $p_param['email'] = $this->request->getPost('email');
  if(!empty($p_param)){
    $Order = new Order();
    $result = $Order->cariShipperID($p_param);
  }
  else{
    $result = '';
  }
  $result = $this->get_results_2($result);
  return json_encode($result, JSON_PRETTY_PRINT);
}


public function getOrderIdTrackAction(){
  $Order = new Order();
  $result = $Order->getOrderIdTrack();
  $result = $this->get_results_2($result);

  return json_encode($result, JSON_PRETTY_PRINT);
}

public function getAllOrderAction(){
  $p_param['email'] = $this->request->getPost('email');
  $Order = new Order();
  $shipperId = $Order->cariShipperID($p_param);
  $result = $Order->getAllOrder($p_param, $shipperId);
  return json_encode($result, JSON_PRETTY_PRINT);
}

public function splitAlamatAction(){
$p_param['Alamat'] = $this->request->getPost('Alamat');
$p_param['shipper_id'] = $this->request->getPost('shipper_id');
$p_param['activity_token'] = $this->getToken(34);

$split = explode(",", $p_param['Alamat']);
$split1 = explode("/", $split[1]);
$split[1] = $split1;

// if(!empty($p_param)){
//   $Order = new Order();
//   $result = $Order->splitAlamat($p_param);
// }else{
//   $result = '';
// }

// $date = "04/30/1973";
// list($month, $day, $year) = split('[/.-]', $date);
// print_r($split);
// $array = $split->fetchArray();
// $i=0;

// if($array){
//   while( $array = $split->fetchArray() ){
//     $data['Alamat'] = $array[0];
//   }
// }
// else{
//   $data['Alamat'] = '';
// }

// $dataResult = $data;

return json_encode($split, JSON_PRETTY_PRINT);
// return json_encode($month, JSON_PRETTY_PRINT);
// return json_encode($result, JSON_PRETTY_PRINT);
}



















































//fungsi tambahan
public function tambahTrackAction(){
  $p_param['order_id'] = $this->request->getPost('order_id');
  $p_param['status'] = $this->request->getPost('status');
  $p_param['agent_id'] = $this->request->getPost('agent_id');

  if(!empty($p_param)){
    $Order = new Order();
    $result = $Order->tambahTrack($p_param);
  }
  else{
    $result = '';
  }
  $result = $this->get_results_2($result);

  return json_encode($result, JSON_PRETTY_PRINT);
}





//simpan form (lokasi pickup dan drop)
  public function simpanFormAction(){
    $Order = new Order();
    $p_param['lokasi_pickup'] = $this->request->getPost('lokasi_pickup');
    $p_param['lokasi_drop'] = $this->request->getPost('lokasi_drop');
    $p_param['shipper_id'] = $this->request->getPost('shipper_id');
    $p_param['activity_token'] = $this->getToken(34);


    if(!empty($p_param)){
      $result1 = $Order->cariDetaiLokasiPickup($p_param);
      $result2 = $Order->cariDetaiLokasiDrop($p_param);
      if($result1 > 0 && $result2 > 0)
      {
        $dataLokasi['lokasi_id_pickup'] = $result1['lokasi_boks_building_id'];//building_pickup_id
        $dataLokasi['lokasi_nama_pickup'] = $result1['lokasi_nama']; //label_pickup
        $dataLokasi['lokasi_address_1_pickup'] = $result1['lokasi_address_1']; //address1_pickup
        $dataLokasi['lokasi_address_2_pickup'] = $result1['lokasi_address_2']; //address2_pickup
        $dataLokasi['lokasi_prov_code_pickup'] = $result1['lokasi_prov_code']; //prov_code_pickup
        $dataLokasi['lokasi_kokab_code_pickup'] = $result1['lokasi_kokab_code']; //kokab_code_pickup
        $dataLokasi['lokasi_kec_code_pickup'] = $result1['lokasi_kec_code']; //kec_code_pickup
        $dataLokasi['lokasi_kel_code_pickup'] = $result1['lokasi_kel_code']; //kec_code_pickup
        $dataLokasi['lokasi_rt_pickup'] = $result1['lokasi_rt']; //rt_pickup
        $dataLokasi['lokasi_rw_pickup'] = $result1['lokasi_rw']; //rw_pickup
        $dataLokasi['lokasi_post_code_pickup'] = $result1['lokasi_post_code']; //post_code_pickup
        $dataLokasi['lokasi_longitude_pickup'] = $result1['lokasi_longitude']; //logitude_pickup
        $dataLokasi['lokasi_latitude_pickup'] = $result1['lokasi_latitude']; //latitude_pickup
        $dataLokasi['lokasi_id_drop'] = $result2['lokasi_boks_building_id'];//building_drop_id
        $dataLokasi['lokasi_nama_drop'] = $result2['lokasi_nama']; //label_drop
        $dataLokasi['lokasi_address_1_drop'] = $result2['lokasi_address_1']; //address1_drop
        $dataLokasi['lokasi_address_2_drop'] = $result2['lokasi_address_2']; //address2_drop
        $dataLokasi['lokasi_prov_code_drop'] = $result2['lokasi_prov_code']; //prov_code_drop
        $dataLokasi['lokasi_kokab_code_drop'] = $result2['lokasi_kokab_code']; //kokab_code_drop
        $dataLokasi['lokasi_kec_code_drop'] = $result2['lokasi_kec_code']; //kec_code_drop
        $dataLokasi['lokasi_kel_code_drop'] = $result2['lokasi_kel_code']; //kec_code_drop
        $dataLokasi['lokasi_rt_drop'] = $result2['lokasi_rt']; //rt_drop
        $dataLokasi['lokasi_rw_drop'] = $result2['lokasi_rw']; //rw_drop
        $dataLokasi['lokasi_post_code_drop'] = $result2['lokasi_post_code']; //post_code_drop
        $dataLokasi['lokasi_longitude_drop'] = $result2['lokasi_longitude']; //logitude_drop
        $dataLokasi['lokasi_latitude_drop'] = $result2['lokasi_latitude']; //latitude_drop
        $dataOrder = $dataLokasi;
      }
      else{
        $dataLokasi['lokasi_id_pickup'] = '';//building_pickup_id
        $dataLokasi['lokasi_nama_pickup'] = ''; //label_pickup
        $dataLokasi['lokasi_address_1_pickup'] = ''; //address1_pickup
        $dataLokasi['lokasi_address_2_pickup'] = ''; //address2_pickup
        $dataLokasi['lokasi_prov_code_pickup'] = ''; //prov_code_pickup
        $dataLokasi['lokasi_kokab_code_pickup'] = ''; //kokab_code_pickup
        $dataLokasi['lokasi_kec_code_pickup'] = ''; //kec_code_pickup
        $dataLokasi['lokasi_kel_code_pickup'] = ''; //kec_code_pickup
        $dataLokasi['lokasi_rt_pickup'] = ''; //rt_pickup
        $dataLokasi['lokasi_rw_pickup'] = ''; //rw_pickup
        $dataLokasi['lokasi_post_code_pickup'] = ''; //post_code_pickup
        $dataLokasi['lokasi_longitude_pickup'] = ''; //logitude_pickup
        $dataLokasi['lokasi_latitude_pickup'] = '';//latitude_pickup
        $dataLokasi['lokasi_id_drop'] = '';//building_drop_id
        $dataLokasi['lokasi_nama_drop'] = ''; //label_drop
        $dataLokasi['lokasi_address_1_drop'] = ''; //address1_drop
        $dataLokasi['lokasi_address_2_drop'] = ''; //address2_drop
        $dataLokasi['lokasi_prov_code_drop'] =''; //prov_code_drop
        $dataLokasi['lokasi_kokab_code_drop'] = ''; //kokab_code_drop
        $dataLokasi['lokasi_kec_code_drop'] = ''; //kec_code_drop
        $dataLokasi['lokasi_kel_code_drop'] =''; //kec_code_drop
        $dataLokasi['lokasi_rt_drop'] = ''; //rt_drop
        $dataLokasi['lokasi_rw_drop'] = ''; //rw_drop
        $dataLokasi['lokasi_post_code_drop'] = ''; //post_code_drop
        $dataLokasi['lokasi_longitude_drop'] = ''; //logitude_drop
        $dataLokasi['lokasi_latitude_drop'] = ''; //latitude_drop

        $dataOrder = $dataLokasi;
      }
      $result = $dataOrder;
    }
    else{
      $result = '';
    }

    $result = $this->get_results_2($result);

    return json_encode($result, JSON_PRETTY_PRINT);
  }





//simpan detail barang
  public function simpanDetailBarangAction(){
    $Order = new Order();
    $percent = 0.5;
    $fileNewName = time();
    $folderPath = "/xampp/htdocs/api/fotoDetailBarang/";

    $p_param['imageupload'] = $this->request->getPost('imageupload');

    $p_param['activity_token'] = $this->getToken(34);

    if(!empty($p_param))
    {
      if(isset($_FILES['imageupload']['tmp_name'])){


        $order_img = base64_encode(file_get_contents($_FILES['imageupload']['tmp_name']));
        $p_param['imageupload'] = $order_img;
        $result = $Order->simpanDetailBarang($p_param) ;

        $file = $_FILES['imageupload']['tmp_name'];
        $ext = pathinfo($_FILES['imageupload']['name'], PATHINFO_EXTENSION);

        if($ext > 0){
          $imageResourceId = imagecreatefromjpg($file);
          imagejpg($imageResourceId,$folderPath. "Detail_Barang". $fileNewName."_thump.".$ext);
        }else{
          $imageResourceId = imagecreatefrompng($file);
          imagepng($imageResourceId,$folderPath. "Detail_Barang". $fileNewName. "_thump.". $ext);
        }
      }
      else{
        $result = ' Tidak ada gambar';
      }
    }

    else
    {
      $result = '';
    }

    $result = $this->get_results_2($result);

    return json_encode($result, JSON_PRETTY_PRINT);
  }


//simpan gambar aja
public function savefotoAction()
{
    $SafefotoModel = new Order();
    $percent = 0.5;
    $fileNewName = time();
    $folderPath = "/xampp/htdocs/api/fotoDetailBarang/";

    if(isset($_FILES['imageupload']['tmp_name'])){

      $file = $_FILES['imageupload']['tmp_name'];
      $ext = pathinfo($_FILES['imageupload']['name'], PATHINFO_EXTENSION);
      $filename = basename( $_FILES['imageupload']['name']);

      $sourceProperties = getimagesize($file);
      $targetLayer = $this->imageResize($file,$sourceProperties[0],$sourceProperties[1]);
      $imageType = $sourceProperties[2];

      if($imageType==IMAGETYPE_JPEG){
        $imageResourceId = imagecreatefromjpeg($file);
        imagejpeg($imageResourceId,$folderPath. "Detail_Barang". $fileNewName."_thump.".$ext);
      }else{
        $imageResourceId = imagecreatefrompng($file);
        imagepng($imageResourceId,$folderPath. "Detail_Barang". $fileNewName. "_thump.". $ext);
      }
      $data = base64_encode($filename);
      $p_param['imageupload'] = $data.$ext;
      $p_param['activity_token'] = $this->getToken(34);


      // $p_param['goods_category'] = $this->request->getPost('goods_category');
      // $p_param['goods_subcategory'] = $this->request->getPost('goods_subcategory');
      // $p_param['goods_dimension_width'] = $this->request->getPost('goods_dimension_width');
      // $p_param['goods_dimension_length'] = $this->request->getPost('goods_dimension_length');
      // $p_param['goods_dimension_height'] = $this->request->getPost('goods_dimension_height');
      // $p_param['goods_dimension_equiweight'] = $this->request->getPost('goods_dimension_equiweight');
      // $p_param['goods_dimension_weight'] = $this->request->getPost('goods_dimension_weight');
      // $p_param['goods_quantity'] = $this->request->getPost('goods_quantity');
      // $p_param['goods_value'] = $this->request->getPost('goods_value');
      // $p_param['description'] = $this->request->getPost('description');
    }


    if(!empty($p_param))
    {
        $result = $SafefotoModel->insert_partner_identity($p_param);
        if($result['status']==true){
            $result['message'] = 'sukses';
            $results = $this->get_results($result);
        }else{
          $result['message'] = 'failed';
          $results = $this->get_results($result);
        }
        return json_encode($results, JSON_PRETTY_PRINT);
    }
}


public function imageResize($imageResourceId,$width,$height)
{
    $targetWidth = 500;
    $diff = $width / $targetWidth;
    $targetHeight = $height / $diff;

    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    return $targetLayer;
}


}
