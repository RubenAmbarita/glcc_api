<?php

/**
 *
 */
 date_default_timezone_set('Asia/Jakarta');

class DashboardAgentModel extends BaseModel
{

  public function getDashboard($param)
  {
    // code...
    $query = "SELECT order_id,partner_id,boks_building_id,email_address,
      order_code,
      pickup_contact_name,
      pickup_address2,
      order_status,
      concat(service_type, ' ', subservice_type) AS service,
      service_type,
      subservice_type,
      pickup_contact_mb,
      drop_contact_name,
      drop_address1,
      drop_contact_mb,
      goods_category,
      goods_subcategory,
      goods_quantity,
      goods_value,
      goods_dimension_length,
      goods_dimension_width,
      goods_dimension_height,
      goods_dimension_equiweight,
      goods_dimension_weight,
      complete_photo
    FROM shipper_order a join boks_partner b
    on a.building_pickup_id = b.boks_building_id where b.partner_id = '".$param['id']."'
    and DATE(a.created_time) = curdate()+0
    ORDER BY (order_status = 'created') DESC";

    $success = $this->db->query($query);
    $i=0;

    if($success){
      while($row = $success->fetchArray())
      {

        $data[$i]['no'] = $i + 1;
        $data[$i]['partner_username'] = $row['email_address'];
        $data[$i]['order_code'] = $row['order_code'];
        $data[$i]['order_id'] = $row['order_id'];
        $data[$i]['partner_id'] = $row['partner_id'];
        $data[$i]['boks_building_id'] = $row['boks_building_id'];
        $data[$i]['pickup_contact_name'] = $row['pickup_contact_name'];
        $data[$i]['pickup_address2'] = $row['pickup_address2'];
        $data[$i]['order_status'] = $row['order_status'];
        $data[$i]['service'] = $row['service'];
        $data[$i]['service_type'] = $row['service_type'];
        $data[$i]['subservice_type'] = $row['subservice_type'];
        $data[$i]['pickup_contact_mb'] = $row['pickup_contact_mb'];
        $data[$i]['drop_contact_name'] = $row['drop_contact_name'];
        $data[$i]['drop_address1'] = $row['drop_address1'];
        $data[$i]['drop_contact_mb'] = $row['drop_contact_mb'];
        $data[$i]['goods_category'] = $row['goods_category'];
        $data[$i]['goods_subcategory'] = $row['goods_subcategory'];
        $data[$i]['goods_quantity'] = $row['goods_quantity'];
        $data[$i]['goods_value'] = $row['goods_value'];
        $data[$i]['goods_dimension_length'] = $row['goods_dimension_length'];
        $data[$i]['goods_dimension_width'] = $row['goods_dimension_width'];
        $data[$i]['goods_dimension_height'] = $row['goods_dimension_height'];
        $data[$i]['goods_dimension_weight'] = $row['goods_dimension_weight'];
        $data[$i]['goods_dimension_equiweight'] = $row['goods_dimension_equiweight'];
        $data[$i]['complete_photo'] = $row['complete_photo'];
        $i = $i+1;
      }
      $status = true;
    }

    else{
      $data['no'] = '';
      $data['order_code'] = '';
      $data['pickup_contact_name'] = '';
      $data['pickup_address2'] = '';
      $data['order_status'] = '';
      $data['service'] = '';
      $data['service_type'] = '';
      $data['subservice_type'] = '';
      $data['pickup_contact_mb'] = '';
      $data['drop_contact_name'] = '';
      $data['drop_address1'] = '';
      $data['drop_contact_mb'] = '';
      $data['goods_category'] = '';
      $data['goods_subcategory'] = '';
      $data['goods_quantity'] = '';
      $data['goods_value'] = '';
      $data['goods_dimension_length'] = '';
      $data['goods_dimension_width'] = '';
      $data['goods_dimension_height'] = '';
      $data['goods_dimension_equiweight'] = '';
      $data['goods_dimension_weight'] = '';
      $data['complete_photo'] = '';
      $status = false;
    }

    $dashboardData = $data;
    $results = array('status' => $status, 'data'=>$dashboardData);
    return $results;
  }
}

 ?>
