<?php

class Product extends BaseModel
{
    public function get_product()
    {
        $query = "SELECT *
                    FROM mdm_productdetail";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['product_seller'] = $row['product_seller'];
            $data[$i]['product_sellername'] = $row['product_sellername'];
            $data[$i]['product_categoryid'] = $row['product_categoryid'];
            $data[$i]['product_categoryname'] = $row['product_categoryname'];
            $data[$i]['product_categorydetailid'] = $row['product_categorydetailid'];
            $data[$i]['product_categorydetailname'] = $row['product_categorydetailname'];
            $data[$i]['product_id'] = $row['product_id'];
            $data[$i]['product_name'] = $row['product_name'];
            $data[$i]['product_information'] = $row['product_information'];
            $data[$i]['product_conditionid'] = $row['product_conditionid'];
            $data[$i]['product_conditionname'] = $row['product_conditionname'];
            $data[$i]['product_price'] = $row['product_price'];
            $data[$i]['product_startdate'] = $row['product_startdate'];
            $data[$i]['product_enddate'] = $row['product_enddate'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];
            
            $i = $i+1;
        }

        $product = $data;
        if(!empty($product))
        {
            $productdiscuss = $this->get_productdiscussion();
            $productpromo = $this->get_productpromo();
            $productreview = $this->get_productreview();
        }
        else {
            $productdiscuss = '';
        }
        //print_r($data);die;
        $result = array('product' => $product, 'productdiscuss' => $productdiscuss, 'productpromo' => $productpromo, 'productreview' => $productreview);

        return $result;
    }

    public function get_productdiscussion()
    {
        $query = "SELECT *
                    FROM mdm_productdiscussion";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['product_seller'] = $row['product_seller'];
            $data[$i]['product_id'] = $row['product_id'];
            $data[$i]['product_name'] = $row['product_name'];
            $data[$i]['product_buyer'] = $row['product_buyer'];
            $data[$i]['product_discusid'] = $row['product_discusid'];
            $data[$i]['product_discusmessage'] = $row['product_discusmessage'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];
            
            $i = $i+1;
        }

        return $data;
    }

    public function get_productpromo()
    {
        $query = "SELECT *
                    FROM mdm_productpromo";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['product_seller'] = $row['product_seller'];
            $data[$i]['product_id'] = $row['product_id'];
            $data[$i]['product_name'] = $row['product_name'];
            $data[$i]['product_promotype'] = $row['product_promotype'];
            $data[$i]['product_discount'] = $row['product_discount'];
            $data[$i]['product_cashback'] = $row['product_cashback'];
            $data[$i]['product_startdate'] = $row['product_startdate'];
            $data[$i]['product_enddate'] = $row['product_enddate'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];
            
            $i = $i+1;
        }

        return $data;
    }

    public function get_productreview()
    {
        $query = "SELECT *
                    FROM mdm_productreview";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['product_seller'] = $row['product_seller'];
            $data[$i]['product_id'] = $row['product_id'];
            $data[$i]['product_name'] = $row['product_name'];
            $data[$i]['product_buyer'] = $row['product_buyer'];
            $data[$i]['product_reviewid'] = $row['product_reviewid'];
            $data[$i]['product_reviewmessage'] = $row['product_reviewmessage'];
            $data[$i]['product_rating'] = $row['product_rating'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];
            
            $i = $i+1;
        }

        return $data;
    }
}
