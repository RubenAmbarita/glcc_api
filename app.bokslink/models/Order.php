<?php

date_default_timezone_set('Asia/Jakarta');


class Order extends BaseModel
{
    //fungsi update Token
    public function update_token($p_token, $p_username)
    {
        $query = "UPDATE shipper
                     SET token = '".$p_token."'
                   WHERE (email_address = '".$p_username."' OR mobile_number = '".$p_username."')";

        $success = $this->db->execute($query);

        if($success > 0)
        {
            $results = true;
        }
        else
        {
            $results = false;
        }
        return $results;
    }

  public function cariDetaiLokasiPickup($p_param, $shipperId){
    $query = " SELECT
                id, name, address_1, address_2, prov_code, kokab_code,
                kec_code, kel_code, rt, rw, post_code, longitude, latitude
              FROM boks_building
              where name = '".$p_param['lokasi_pickup']."'
              ";
    $result = $this->db->query($query);

    $row = $result->fetchArray();

    if($row['id'])
    {
      $data['type'] = 'Pickup';
      $data['lokasi_boks_building_id'] = $row['id'];
      $data['lokasi_nama'] = $row['name'];
      $data['lokasi_address_1'] = $row['address_1'];
      $data['lokasi_address_2'] = $row['address_2'];
      $data['lokasi_prov_code'] = $row['prov_code'];
      $data['lokasi_kokab_code'] = $row['kokab_code'];
      $data['lokasi_kec_code'] = $row['kec_code'];
      $data['lokasi_kel_code'] = $row['kel_code'];
      $data['lokasi_rt'] = $row['rt'];
      $data['lokasi_rw'] = $row['rw'];
      $data['lokasi_post_code'] = $row['post_code'];
      $data['lokasi_longitude'] = $row['longitude'];
      $data['lokasi_latitude'] = $row['latitude'];
      $status = 'true';
    }
    else{
      $data['type'] = '';
      $data['lokasi_boks_building_id'] = '';
      $data['lokasi_nama'] = '';
      $data['lokasi_address_1'] = '';
      $data['lokasi_address_2'] = '';
      $data['lokasi_prov_code'] = '';
      $data['lokasi_kokab_code'] = '';
      $data['lokasi_kec_code'] = '';
      $data['lokasi_kel_code'] = '';
      $data['lokasi_rt'] = '';
      $data['lokasi_rw'] = '';
      $data['lokasi_post_code'] = '';
      $data['lokasi_longitude'] = '';
      $data['lokasi_latitude'] = '';
      $status = 'false';
    }

      if($status == 'true'){
        $lokasi_pickup = $data;
        $simpan_lokasi_Pickup = $this->simpanLokasi($lokasi_pickup, $p_param, $shipperId);
      }
      else{
        $lokasiPickup = '';
        $simpan_lokasi_Pickup = '';
      }

    return $lokasi_pickup;
  }


  public function cariDetaiLokasiDrop($p_param, $shipperId){
    $query = " SELECT
                id, name, address_1, address_2, prov_code, kokab_code,
                kec_code, kel_code, rt, rw, post_code, longitude, latitude
              FROM boks_building
              where name = '".$p_param['lokasi_drop']."'
              ";
    $result = $this->db->query($query);

    $row = $result->fetchArray();

    if($row['id'])
    {
      $data['type'] = 'Drop';
      $data['lokasi_boks_building_id'] = $row['id'];
      $data['lokasi_nama'] = $row['name'];
      $data['lokasi_address_1'] = $row['address_1'];
      $data['lokasi_address_2'] = $row['address_2'];
      $data['lokasi_prov_code'] = $row['prov_code'];
      $data['lokasi_kokab_code'] = $row['kokab_code'];
      $data['lokasi_kec_code'] = $row['kec_code'];
      $data['lokasi_kel_code'] = $row['kel_code'];
      $data['lokasi_rt'] = $row['rt'];
      $data['lokasi_rw'] = $row['rw'];
      $data['lokasi_post_code'] = $row['post_code'];
      $data['lokasi_longitude'] = $row['longitude'];
      $data['lokasi_latitude'] = $row['latitude'];
      $status = 'true';
    }
    else{
      $data['type'] = 'Drop';
      $data['lokasi_boks_building_id'] = '';
      $data['lokasi_nama'] = '';
      $data['lokasi_address_1'] = '';
      $data['lokasi_address_2'] = '';
      $data['lokasi_prov_code'] = '';
      $data['lokasi_kokab_code'] = '';
      $data['lokasi_kec_code'] = '';
      $data['lokasi_kel_code'] = '';
      $data['lokasi_rt'] = '';
      $data['lokasi_rw'] = '';
      $data['lokasi_post_code'] = '';
      $data['lokasi_longitude'] = '';
      $data['lokasi_latitude'] = '';
      $status = 'false';
    }

    if($status == 'true'){
      $lokasi_drop = $data;
      $simpan_lokasi_Drop = $this->simpanLokasi($lokasi_drop, $p_param, $shipperId);
    }
    else{
      $lokasi_drop = '';
      $simpan_lokasi_Drop = '';
    }

    return $lokasi_drop;
  }




  public function simpanLokasi($result, $p_param, $shipperId){

    $currentDate = date('Y-m-d');
    $currentDateTime = date('Y-m-d H:i:s');


    $query = " INSERT INTO shipper_location
                (
                  shipper_id,
                  type,
                  label,
                  address1,
                  address2,
                  address3,
                  prov_code,
                  kokab_code,
                  kec_code,
                  kel_code,
                  rt,
                  rw,
                  post_code,
                  logitude,
                  latitude,
                  created_time,
                  updated_time,
                  token
                )
              VALUES
                (
                  '".$shipperId['shipper_id']."',
                  '".$result['type']."',
                  '".$result['lokasi_nama']."',
                  '".$result['lokasi_address_1']."',
                  '".$result['lokasi_address_2']."',
                  '',
                  '".$result['lokasi_prov_code']."',
                  '".$result['lokasi_kokab_code']."',
                  '".$result['lokasi_kec_code']."',
                  '".$result['lokasi_kel_code']."',
                  '".$result['lokasi_rt']."',
                  '".$result['lokasi_rw']."',
                  '".$result['lokasi_post_code']."',
                  '".$result['lokasi_longitude']."',
                  '".$result['lokasi_latitude']."',
                  '$currentDateTime',
                  '$currentDateTime',
                  '".$p_param['activity_token']."'
                )";

            $success = $this->db->execute($query);

            if($success > 0)
            {
                $status = 'true';
                $data['status Lokasi'] = 'Berhasil disimpan';
            }
            else
            {
                $status = 'false';
                $data['status Lokasi'] = '';
            }

        $StatusLokasi = $data;

        $result = array('status' => $status, 'Status Lokasi' => $StatusLokasi);

        return $result;
  }


  public function simpanDetailOrder($dataOrder, $p_param, $shipperId){
    $currentDateTime = date('Y-m-d H:i:s');

    $query = " INSERT INTO shipper_order
                (
                  order_code,
                  shipper_id,
                  service_type,
                  subservice_type,
                  order_status,
                  building_pickup_id,
                  pickup_address1,
                  pickup_address2,
                  pickup_address3,
                  pickup_prov_code,
                  pickup_kokab_code,
                  pickup_kec_code,
                  pickup_kel_code,
                  pickup_rt,
                  pickup_rw,
                  pickup_post_code,
                  pickup_contact_name,
                  pickup_contact_mb,
                  pickup_longitude,
                  pickup_latitude,
                  receiver_name,
                  building_drop_id,
                  drop_address1,
                  drop_address2,
                  drop_address3,
                  drop_prov_code,
                  drop_kokab_code,
                  drop_kec_code,
                  drop_kel_code,
                  drop_rt,
                  drop_rw,
                  drop_post_code,
                  drop_contact_name,
                  drop_contact_mb,
                  drop_longitude,
                  drop_latitude,
                  goods_category,
                  goods_subcategory,
                  goods_dimension_width,
                  goods_dimension_length,
                  goods_dimension_height,
                  goods_dimension_equiweight,
                  goods_dimension_weight,
                  goods_quantity,
                  goods_value,
                  complete_photo,
                  description,
                  is_insured,
                  insurance_amount,
                  charge_amount,
                  kodePromo,
                  payment_method,
                  created_time,
                  status_time,
                  cancel_by,
                  cancel_reason,
                  cancel_time,
                  first_agent_id,
                  second_agent_id,
                  third_agent_id,
                  trip_id,
                  courier_id,
                  pickup_assignment_photo,
                  shipper_goods_photo,
                  complete_time,
                  token
                )
              VALUES
                (
                  '".$p_param['order_code']."',
                  '".$shipperId['shipper_id']."',
                  '".$p_param['service_type']."',
                  '".$p_param['subservice_type']."',
                  'Created',
                  '".$dataOrder['lokasi_id_pickup']."',
                  '".$dataOrder['lokasi_address_1_pickup']."',
                  '".$dataOrder['lokasi_address_2_pickup']."',
                  'address3_pickup',
                  '".$dataOrder['lokasi_prov_code_pickup']."',
                  '".$dataOrder['lokasi_kokab_code_pickup']."',
                  '".$dataOrder['lokasi_kec_code_pickup']."',
                  '".$dataOrder['lokasi_kel_code_pickup']."',
                  '".$dataOrder['lokasi_rt_pickup']."',
                  '".$dataOrder['lokasi_rw_pickup']."',
                  '".$dataOrder['lokasi_post_code_pickup']."',
                  '".$p_param['nama_pengirim']."',
                  '".$p_param['mp_pengirim']."',
                  '".$dataOrder['lokasi_longitude_pickup']."',
                  '".$dataOrder['lokasi_latitude_pickup']."',
                  'receiver_name',
                  '".$dataOrder['lokasi_id_drop']."',
                  '".$dataOrder['lokasi_address_1_drop']."',
                  '".$dataOrder['lokasi_address_2_drop']."',
                  'address3_drop',
                  '".$dataOrder['lokasi_prov_code_drop']."',
                  '".$dataOrder['lokasi_kokab_code_drop']."',
                  '".$dataOrder['lokasi_kec_code_drop']."',
                  '".$dataOrder['lokasi_kel_code_drop']."',
                  '".$dataOrder['lokasi_rt_drop']."',
                  '".$dataOrder['lokasi_rw_drop']."',
                  '".$dataOrder['lokasi_post_code_drop']."',
                  '".$p_param['nama_penerima']."',
                  '".$p_param['mp_penerima']."',
                  '".$dataOrder['lokasi_longitude_pickup']."',
                  '".$dataOrder['lokasi_latitude_pickup']."',
                  '".$p_param['goods_category']."',
                  '".$p_param['goods_subcategory']."',
                  '".$p_param['goods_dimension_width']."',
                  '".$p_param['goods_dimension_length']."',
                  '".$p_param['goods_dimension_height']."',
                  '".$p_param['goods_dimension_equiweight']."',
                  '".$p_param['goods_dimension_weight']."',
                  '".$p_param['goods_quantity']."',
                  '".$p_param['goods_value']."',
                  '".$p_param['imageupload']."',
                  '".$p_param['description']."',
                  '".$p_param['is_insured']."',
                  '".$p_param['insurance_amount']."',
                  '".$p_param['charge_amount']."',
                  '".$p_param['kodePromo']."',
                  '".$p_param['payment_method']."',
                  '".$currentDateTime."',
                  'status_time',
                  'cancel_by',
                  'cancel_reason',
                  'cancel_time',
                  '0',
                  '0',
                  '0',
                  '0',
                  '0',
                  'pickup_assignment_photo',
                  'shipper_goods_photo',
                  'complete_time',
                  '".$p_param['activity_token']."'
                )";

            $success = $this->db->execute($query);

            $mei = $this->cariDetailOrderTrack($p_param);

            // $query2 = " SELECT id, status, first_agent_id, created_time FROM shipper_order WHERE order_code = '".$p_param['order_code']."' ";


            if($success > 0)
            {
                $status = true;
                $data['status'] = 'true';
            }
            else
            {
                $status = false;
                $data['status'] = 'false';
            }

        $StatusOrder = $data;

        $result = array('status' => $status, 'data' => $StatusOrder);

        return $result;
  }




  public function cariDetailOrderTrack($p_param){
    $query = " SELECT
                order_id, created_time, order_status, first_agent_id
              FROM shipper_order
              where order_code = '".$p_param['order_code']."'
              ";
    $result = $this->db->query($query);

    $row = $result->fetchArray();

    if($row['order_id'])
    {
      $data['order_id'] = $row['order_id'];
      $data['order_created_time'] = $row['created_time'];
      $data['order_status'] = $row['order_status'];
      $data['order_first_agent_id'] = $row['first_agent_id'];
      $status = 'true';
    }
    else{
      $data['order_id'] = '';
      $data['order_created_time'] = '';
      $data['order_status'] = '';
      $data['order_first_agent_id'] = '';
      $status = 'false';
    }

    if($status == 'true'){
      $lokasi_drop = $data;
      $simpan_Detail_Order_Track = $this->simpanDetailOrderTrack($lokasi_drop);
    }
    else{
      $lokasi_drop = '';
      $simpan_Detail_Order_Track = '';
    }

    return $lokasi_drop;
  }




  public function simpanDetailOrderTrack($result){
    $query = " INSERT INTO shipper_order_track
                (
                  shipper_order_id,
                  track_time,
                  status,
                  agent_id
                )
              VALUES
                (
                  '".$result['order_id']."',
                  '".$result['order_created_time']."',
                  '".$result['order_status']."',
                  '".$result['order_first_agent_id']."'
                )
                ";

            $success = $this->db->execute($query);

            if($success > 0)
            {
                $status = 'true';
                $data['status order track'] = 'Berhasil disimpan';
            }
            else
            {
                $status = 'false';
                $data['status order track'] = '';
            }

        $StatusLokasi = $data;

        $result = array('status' => $status, 'Status Order' => $StatusLokasi);

        return $result;
  }

public function cariShipperID($p_param){
  $query = " SELECT id
            FROM shipper
            where email_address = '".$p_param['email']."'
            ";
    $result = $this->db->query($query);

    $row = $result->fetchArray();

    if($row['id'])
    {
    $data['shipper_id'] = $row['id'];
    $status = 'true';
    }
    else{
    $data['shipper_id'] = '';
    $status = 'false';
    }

    if($status == 'true'){
    $lokasi_drop = $data;
    }
    else{
    $lokasi_drop = '';
    }

    return $lokasi_drop;
}


public function getOrderIdTrack(){
  $query = " SELECT order_id, order_code
              FROM shipper_order
              WHERE shipper_id = '168'
  ";
      $result = $this->db->query($query);
      $row = $result->fetchArray();
      $i=0;
      if($row > 0){
        while($row = $result->fetchArray()){
          $data[$i]['id'] = $row['order_id'];
          $data[$i]['order_code'] = $row['order_code'];
          $data[$i]['order_track'] = $this->getOrderTrack($data[$i]['order_id']);
          // $data[$i]['order_track'] = $this->getOrderTrack( '251' );
          $i = $i+1;
        }
        $status = true;
      }
      else{
        $data['id'] = '';
        $data[$i]['order_code'] = '';
        $data[$i]['order_track'] = '';
        $status = false;
      }
      $dataOrder = $data;
      $results = $dataOrder;
      return $results;
}

public function getOrderTrack($p_param){
  $query = " SELECT shipper_order_id, track_time, status, agent_id
              FROM shipper_order_track
              WHERE shipper_order_id = '".$p_param."'
  ";

      $result = $this->db->query($query);
      $row = $result->fetchArray();
      $i=0;

      if($row['shipper_order_id']){
        while($row = $result->fetchArray()){
          $data[$i]['no'] = $i + 1;
          $data[$i]['shipper_order_id'] = $row['shipper_order_id'];
          $data[$i]['track_time'] = $row['track_time'];
          $data[$i]['status'] = $row['status'];
          $data[$i]['agent_id'] = $row['agent_id'];
          $i = $i+1;
        }
        $status = true;
      }
      else{
        $data[$i]['no'] = '';
        $data[$i]['shipper_order_id'] = '';
        $data[$i]['track_time'] = '';
        $data[$i]['status'] = '';
        $data[$i]['agent_id'] = '';
        $status = false;
      }

      $dataOrder = $data;
      $results = $dataOrder;
      return $results;
}


// public function getOrderTrack($p_param){
//   $query = " SELECT shipper_order_id, track_time, status, agent_id
//               FROM shipper_order_track
//               WHERE shipper_order_id = '".$p_param."'
//   ";

//       $result = $this->db->query($query);
//       $row = $result->fetchArray();
//       $i=0;

//       if($row > 0){
//         while($row = $result->fetchArray()){
//         $data[$i]['shipper_order_id'] = $row['shipper_order_id'];
//         $data[$i]['track_time'] = $row['track_time'];
//         $data[$i]['status'] = $row['status'];
//         $data[$i]['agent_id'] = $row['agent_id'];
//         $i = $i+1;
//         }
//         $status = true;
//         }
//         else{
//           $data[$i]['shipper_order_id'] = '';
//           $data[$i]['track_time'] = '';
//           $data[$i]['status'] = '';
//           $data[$i]['agent_id'] = '';
//         $status = false;
//         }
//         $dataLocation = $data;
//         $results = $dataLocation;
//         return $results;
// }


public function getAllOrder($p_param, $shipperId){
  $query= " SELECT
              order_id,
              order_code,
              service_type,
              subservice_type,
              order_status,
              pickup_contact_name,
              pickup_contact_mb,
              pickup_address2,
              drop_contact_name,
              drop_contact_mb,
              drop_address2,
              goods_category,
              goods_subcategory,
              goods_dimension_width,
              goods_dimension_length,
              goods_dimension_height,
              goods_dimension_equiweight,
              goods_dimension_weight,
              goods_quantity,
              goods_value,
              complete_photo,
              description,
              charge_amount,
              kodePromo,
              payment_method
            FROM shipper_order
            WHERE shipper_id = '168'
            ORDER BY order_id DESC
  ";

  $result = $this->db->query($query);
  $row = $result->fetchArray();
  $i=0;

  if($row['order_id']){
    while($row = $result->fetchArray()){
      $data[$i]['no'] = $i + 1;
      $data[$i]['order_id'] = $row['order_id'];
      $data[$i]['order_code'] = $row['order_code'];
      $data[$i]['service_type'] = $row['service_type'];
      $data[$i]['subservice_type'] = $row['subservice_type'];
      $data[$i]['order_status'] = $row['order_status'];
      $data[$i]['pickup_contact_name'] = $row['pickup_contact_name'];
      $data[$i]['pickup_contact_mb'] = $row['pickup_contact_mb'];
      $data[$i]['pickup_address2'] = $row['pickup_address2'];
      $data[$i]['drop_contact_name'] = $row['drop_contact_name'];
      $data[$i]['drop_contact_mb'] = $row['drop_contact_mb'];
      $data[$i]['drop_address2'] = $row['drop_address2'];
      $data[$i]['goods_category'] = $row['goods_category'];
      $data[$i]['goods_subcategory'] = $row['goods_subcategory'];
      $data[$i]['goods_dimension_width'] = $row['goods_dimension_width'];
      $data[$i]['goods_dimension_length'] = $row['goods_dimension_length'];
      $data[$i]['goods_dimension_height'] = $row['goods_dimension_height'];
      $data[$i]['goods_dimension_equiweight'] = $row['goods_dimension_equiweight'];
      $data[$i]['goods_dimension_weight'] = $row['goods_dimension_weight'];
      $data[$i]['goods_quantity'] = $row['goods_quantity'];
      $data[$i]['goods_value'] = $row['goods_value'];
      $data[$i]['complete_photo'] = $row['complete_photo'];
      $data[$i]['description'] = $row['description'];
      $data[$i]['charge_amount'] = $row['charge_amount'];
      $data[$i]['kodePromo'] = $row['kodePromo'];
      $data[$i]['payment_method'] = $row['payment_method'];
      $i = $i+1;
    }
    $status = true;
  }
  else{
    $data[$i]['no'] = '';
    $data[$i]['order_id'] = '';
    $data[$i]['order_code'] = '';
    $data[$i]['service_type'] = '';
    $data[$i]['subservice_type'] = '';
    $data[$i]['order_status'] = '';
    $data[$i]['pickup_contact_name'] = '';
    $data[$i]['pickup_contact_mb'] = '';
    $data[$i]['pickup_address2'] = '';
    $data[$i]['drop_contact_name'] = '';
    $data[$i]['drop_contact_mb'] = '';
    $data[$i]['drop_address2'] = '';
    $data[$i]['goods_category'] = '';
    $data[$i]['goods_subcategory'] = '';
    $data[$i]['goods_dimension_width'] = '';
    $data[$i]['goods_dimension_length'] = '';
    $data[$i]['goods_dimension_height'] = '';
    $data[$i]['goods_dimension_equiweight'] = '';
    $data[$i]['goods_dimension_weight'] = '';
    $data[$i]['goods_quantity'] = '';
    $data[$i]['goods_value'] = '';
    $data[$i]['complete_photo'] = '';
    $data[$i]['description'] = '';
    $data[$i]['charge_amount'] = '';
    $data[$i]['kodePromo'] = '';
    $data[$i]['payment_method'] = '';
    $status = false;
  }

  $dataOrder = $data;
  $results = $dataOrder;
  return $results;

}

public function splitAlamat($p_param){
  $split = explode(",", $p_param['Alamat']);
  $currentDateTime = date('Y-m-d H:i:s');
  $query = " INSERT INTO shipper_location
              (
                shipper_id,
                type,
                label,
                address1,
                address2,
                address3,
                prov_code,
                kokab_code,
                kec_code,
                kel_code,
                rt,
                rw,
                post_code,
                logitude,
                latitude,
                created_time,
                updated_time,
                token
              )
            VALUES
            (
              '".$p_param['shipper_id']."',
              'Pickup',
              '".$split[0]."',
              '".$split[0]."',
              'address2',
              'address3',
              '".$split[5]."',
              '".$split[4]."',
              '".$split[3]."',
              '".$split[2]."',
              '".$split[1]."',
              '".$split[1]."',
              'post_code',
              'logitude',
              'latitude',
              '".$currentDateTime."',
              '".$currentDateTime."',
              '".$p_param['activity_token']."'
            )
  ";

$success = $this->db->query($query);

if($success)
{
    $status = true;
    $data['status'] = 'true';
}
else
{
    $status = false;
    $data['status'] = 'false';
}

$StatusOrder = $data;

$result = array('status' => $status, 'data' => $StatusOrder);

return $result;

}





























//fungsi tambahan
public function tambahTrack($p_param){
  $currentDateTime = date('Y-m-d H:i:s');
  $query = " INSERT INTO shipper_order_track
              (
                shipper_order_id,
                track_time,
                status,
                agent_id
              )
              VALUES
              (
                '".$p_param['order_id']."',
               '".$currentDateTime."',
                '".$p_param['status']."',
                '".$p_param['agent_id']."'
              )
  ";

$success = $this->db->execute($query);

if($success > 0)
{
    $status = true;
    $data['status Order'] = 'true';
}
else
{
    $status = false;
    $data['status Order'] = 'false';
}

$StatusOrder = $data;

$result = array('status' => $status, 'data' => $StatusOrder);

return $result;


}





























  public function simpanDetailBarang($p_param){
    $currentDateTime = date('Y-m-d H:i:s');

    $query = " INSERT INTO shipper_order
    (
      order_code,
      shipper_id,
      service_type,
      subservice_type,
      status,
      building_pickup_id,
      pickup_address1,
      pickup_address2,
      pickup_address3,
      pickup_prov_code,
      pickup_kokab_code,
      pickup_kec_code,
      pickup_kel_code,
      pickup_rt,
      pickup_rw,
      pickup_post_code,
      pickup_contact_name,
      pickup_contact_mb,
      pickup_longitude,
      pickup_latitude,
      receiver_name,
      building_drop_id,
      drop_address1,
      drop_address2,
      drop_address3,
      drop_prov_code,
      drop_kokab_code,
      drop_kec_code,
      drop_kel_code,
      drop_rt,
      drop_rw,
      drop_post_code,
      drop_contact_name,
      drop_contact_mb,
      drop_longitude,
      drop_latitude,
      goods_category,
      goods_subcategory,
      goods_dimension_width,
      goods_dimension_length,
      goods_dimension_height,
      goods_dimension_equiweight,
      goods_dimension_weight,
      goods_quantity,
      goods_value,
      complete_photo,
      description,
      is_insured,
      insurance_amount,
      charge_amount,
      kodePromo,
      payment_method,
      created_time,
      status_time,
      cancel_by,
      cancel_reason,
      cancel_time,
      first_agent_id,
      second_agent_id,
      third_agent_id,
      trip_id,
      courier_id,
      pickup_assignment_photo,
      shipper_goods_photo,
      complete_time,
      token
    )
  VALUES
    (
      ' order code ',
      ' shipper id ',
      ' service type ',
      ' subservice type ',
      ' Created ',
      ' lokasi pickup id ',
      ' address 1 pickup ',
      ' address 2 pickup ',
      ' address3_pickup ',
      ' prov code pickup ',
      ' kokab code pickup ',
      ' kec code pickup ',
      ' kel code pickup ',
      ' rt pickup ',
      ' rw pickup ',
      ' post code pickup ',
      ' nama pengirim ',
      ' nomor pengirim ',
      ' longitude pickup ',
      ' latitude pickup ',
      ' receiver_name ',
      ' lokasi drop id ',
      ' address 1 drop ',
      ' address 2 drop ',
      ' address3_drop ',
      ' prov code drop ',
      ' kokab code drop ',
      ' kec code drop ',
      ' kel drop code ',
      ' rt drop code ',
      ' rw drop code ',
      ' post code drop ',
      ' nama penerima ',
      ' nomor penerima ',
      ' longitude penerima ',
      ' latitude penerima ',
      ' goods category ',
      ' goods subcategory  ',
      ' goods dimension width ',
      ' goods dimension length ',
      ' goods dimension height ',
      ' goods dimensioan equiweigth',
      ' goods dimension weight ',
      ' goods quantity ',
      ' goods value ',
      ' ".$p_param['imageupload']." ',
      ' goods description ',
      ' goods is insured ',
      ' insurance amount ',
      ' charge amount ',
      ' payment method ',
      ' ".$currentDateTime." ',
      ' status_time ',
      ' cancel_by ',
      ' cancel_reason ',
      ' cancel_time ',
      ' first_agent_id ',
      ' second_agent_id ',
      ' third_agent_id ',
      ' trip_id' ,
      ' courier_id ',
      'pickup_assignment_photo',
      'shipper_goods_photo',
      ' complete_time ',
      ' ".$p_param['activity_token']." '
    )";


$success = $this->db->execute($query);


if($success > 0)
{
    $status = true;
    $data['status Order'] = 'true';
}
else
{
    $status = false;
    $data['status Order'] = 'false';
}

$StatusOrder = $data;

$result = array('status' => $status, 'data' => $StatusOrder);

return $result;


  }





  public function insert_partner_identity($p_param)
  {
    $currentDateTime = date('Y-m-d H:i:s');

    $query = " INSERT INTO shipper_order
    (
      order_code,
      shipper_id,
      service_type,
      subservice_type,
      status,
      building_pickup_id,
      pickup_address1,
      pickup_address2,
      pickup_address3,
      pickup_prov_code,
      pickup_kokab_code,
      pickup_kec_code,
      pickup_kel_code,
      pickup_rt,
      pickup_rw,
      pickup_post_code,
      pickup_contact_name,
      pickup_contact_mb,
      pickup_longitude,
      pickup_latitude,
      receiver_name,
      building_drop_id,
      drop_address1,
      drop_address2,
      drop_address3,
      drop_prov_code,
      drop_kokab_code,
      drop_kec_code,
      drop_kel_code,
      drop_rt,
      drop_rw,
      drop_post_code,
      drop_contact_name,
      drop_contact_mb,
      drop_longitude,
      drop_latitude,
      goods_category,
      goods_subcategory,
      goods_dimension_width,
      goods_dimension_length,
      goods_dimension_height,
      goods_dimension_equiweight,
      goods_dimension_weight,
      goods_quantity,
      goods_value,
      complete_photo,
      description,
      is_insured,
      insurance_amount,
      charge_amount,
      kodePromo,
      payment_method,
      created_time,
      status_time,
      cancel_by,
      cancel_reason,
      cancel_time,
      first_agent_id,
      second_agent_id,
      third_agent_id,
      trip_id,
      courier_id,
      pickup_assignment_photo,
      shipper_goods_photo,
      complete_time,
      token
    )
  VALUES
    (
      ' order code ',
      ' shipper id ',
      ' service type ',
      ' subservice type ',
      ' Created ',
      ' lokasi pickup id ',
      ' address 1 pickup ',
      ' address 2 pickup ',
      ' address3_pickup ',
      ' prov code pickup ',
      ' kokab code pickup ',
      ' kec code pickup ',
      ' kel code pickup ',
      ' rt pickup ',
      ' rw pickup ',
      ' post code pickup ',
      ' nama pengirim ',
      ' nomor pengirim ',
      ' longitude pickup ',
      ' latitude pickup ',
      ' receiver_name ',
      ' lokasi drop id ',
      ' address 1 drop ',
      ' address 2 drop ',
      ' address3_drop ',
      ' prov code drop ',
      ' kokab code drop ',
      ' kec code drop ',
      ' kel drop code ',
      ' rt drop code ',
      ' rw drop code ',
      ' post code drop ',
      ' nama penerima ',
      ' nomor penerima ',
      ' longitude penerima ',
      ' latitude penerima ',
      ' goods_category ',
      ' goods_subcategory  ',
      ' goods_dimension_width ',
      ' goods_dimension_length ',
      ' goods_dimension_height ',
      ' goods_dimension_equiweight ',
      ' goods_dimension_weight ',
      ' goods_quantity ',
      ' goods_value ',
      ' ".$p_param['imageupload']." ',
      ' description ',
      ' goods is insured ',
      ' insurance amount ',
      ' charge amount ',
      ' payment method ',
      ' ".$currentDateTime." ',
      ' status_time ',
      ' cancel_by ',
      ' cancel_reason ',
      ' cancel_time ',
      ' first_agent_id ',
      ' second_agent_id ',
      ' third_agent_id ',
      ' trip_id' ,
      ' courier_id ',
      ' pickup_assignment_photo ',
      ' shipper_goods_photo ',
      ' complete_time ',
      ' ".$p_param['activity_token']." '
    )";

      $success = $this->db->execute($query);

      if($success > 0)
      {
          $status = true;
      }
      else
      {
          $status = false;
      }
      $hasil = array('status' => $status);
      return $hasil;
  }

}

 ?>
