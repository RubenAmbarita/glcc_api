<?php

class Api extends BaseModel
{
    public function update_token($p_token, $p_username)
    {
        $query = "UPDATE trucker_driver
                     SET token = '".$p_token."'
                   WHERE (username = '".$p_username."' OR email = '".$p_username."' OR no_telp = '".$p_username."')";

        $success = $this->db->execute($query);
        
        if($success > 0)
        {
            $results = true;
        }
        else
        {
            $results = false;
        }
        return $results;
    }

    public function get_auth($p_param)
    {
        $query = "SELECT t1.id_driver AS driver_id,
                         t1.name AS driver_name,
                         CONCAT('"."http://trucker.bokscube.co.id/assets/driver/"."', t1.image_driver) AS driver_image,
                         t1.id_trucker AS trucker_id,
                         (SELECT t2.company_name FROM trucker t2 WHERE t1.id_trucker = t2.id_trucker) AS trucker_name
                    FROM trucker_driver t1
                   WHERE (t1.username = '".$p_param['username']."' OR t1.email = '".$p_param['username']."' OR t1.no_telp = '".$p_param['username']."')
                     AND t1.password = '".$p_param['password']."' ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['driver_id'])
        {
            $data['driver_id'] = $row['driver_id'];
            $data['driver_name'] = $row['driver_name'];
            $data['driver_image'] = $row['driver_image'];
            $data['trucker_id'] = $row['trucker_id'];
            $data['trucker_name'] = $row['trucker_name'];
        }

        $profile = $data;

        if(!empty($profile))
        {
            $order = $this->get_order($data['driver_id']);
            $status = true;
        }
        else {
            $order = '';
            $status = false;
        }

        $result = array('status' => $status, 'profile' => $profile, 'order' => $order);

        return $result;
    }

    public function get_order($p_driverid)
    {
        $query = "SELECT t1.order_id AS OrderId,
                         t1.order_detailid AS OrderDetailId,
                         (SELECT t2.order_ponumber FROM shipper_order t2 WHERE t1.order_id = t2.order_id) AS PONomber,
                         t1.order_shipmentnumber AS ShipmentNumber,
                         (SELECT t2.no_polisi
                            FROM trucker_fleet t2
                           WHERE t1.order_assigntruckfleetid = t2.id_truck_fleet)
                            AS PoliceNumber,
                         'PT. INDOFOOD INDONESIA' AS ShipperName,
                         'GUDANG MODENA INDONESIA' AS OriginName,
                         t1.order_originaddress AS OriginAddress,
                         t1.order_origincoordinate AS OriginCoordinate,
                         'Edy SW' AS OriginPICName,
                         '62811900091' AS OriginPICPhone,
                         t1.order_destinationname AS DestinationName,
                         t1.order_destinationaddress AS DestinationAddress,
                         t1.order_destinationcoordinate AS DestinationCoordinate,
                         'Meyyer' AS DestinationPICName,
                         '6281190034564' AS DestinationPICPhone,
                         t1.order_quantity AS Qty,
                         CASE WHEN t1.order_weightvolume > t1.order_weightactual THEN
                             t1.order_weightvolume
                         ELSE
                             t1.order_weightactual
                         END
                            AS Weight,
                         t1.order_status AS StatusId,
                         (SELECT t2.mst_codedesc
                            FROM trucker_mstcodedetail t2
                           WHERE t2.mst_codegroup = 'stodr'
                             AND t2.mst_codedetail = t1.order_status) AS StatusName
                    FROM shipper_orderdetail t1
                   WHERE t1.order_assigndriverid = '".$p_driverid."'
                     AND t1.order_status != 6 ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['PoliceNumber'])
        {
            $data['OrderId'] = $row['OrderId'];
            $data['OrderDetailId'] = $row['OrderDetailId'];
            $data['PONomber'] = $row['PONomber'];
            $data['ShipmentNumber'] = $row['ShipmentNumber'];
            $data['PoliceNumber'] = $row['PoliceNumber'];
            $data['ShipperName'] = $row['ShipperName'];
            $data['OriginName'] = $row['OriginName'];
            $data['OriginAddress'] = $row['OriginAddress'];
            $data['OriginCoordinate'] = $row['OriginCoordinate'];
            $data['OriginPICName'] = $row['OriginPICName'];
            $data['OriginPICPhone'] = $row['OriginPICPhone'];
            $data['DestinationName'] = $row['DestinationName'];
            $data['DestinationAddress'] = $row['DestinationAddress'];
            $data['DestinationCoordinate'] = $row['DestinationCoordinate'];
            $data['DestinationPICName'] = $row['DestinationPICName'];
            $data['DestinationPICPhone'] = $row['DestinationPICPhone'];
            $data['Qty'] = $row['Qty'];
            $data['Weight'] = $row['Weight'];
            $data['StatusId'] = $row['StatusId'];
            $data['StatusName'] = $row['StatusName'];
        }

        return $data;
    }

    public function get_profileaddress($p_param)
    {
        $query = "SELECT *
                    FROM mdm_usersaddress
                   WHERE user_id = '".$p_param."'";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['user_id'] = $row['user_id'];
            $data[$i]['user_name'] = $row['user_name'];
            $data[$i]['user_locationid'] = $row['user_locationid'];
            $data[$i]['user_locationname'] = $row['user_locationname'];
            $data[$i]['user_pinpoint'] = $row['user_pinpoint'];
            $data[$i]['user_address'] = $row['user_address'];
            $data[$i]['user_citydistrict'] = $row['user_citydistrict'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];
            
            $i = $i+1;
        }

        return $data;
    }

    public function get_neworder($p_param)
    {
        $query = "SELECT t1.order_id AS OrderId,
                         t1.order_detailid AS OrderDetailId,
                         (SELECT t2.order_ponumber FROM shipper_order t2 WHERE t1.order_id = t2.order_id) AS PONomber,
                         t1.order_shipmentnumber AS ShipmentNumber,
                         (SELECT t2.no_polisi
                            FROM trucker_fleet t2
                           WHERE t1.order_assigntruckfleetid = t2.id_truck_fleet)
                            AS PoliceNumber,
                         'PT. INDOFOOD INDONESIA' AS ShipperName,
                         'GUDANG MODENA INDONESIA' AS OriginName,
                         t1.order_originaddress AS OriginAddress,
                         t1.order_origincoordinate AS OriginCoordinate,
                         'Edy SW' AS OriginPICName,
                         '62811900091' AS OriginPICPhone,
                         t1.order_destinationname AS DestinationName,
                         t1.order_destinationaddress AS DestinationAddress,
                         t1.order_destinationcoordinate AS DestinationCoordinate,
                         'Meyyer' AS DestinationPICName,
                         '6281190034564' AS DestinationPICPhone,
                         t1.order_quantity AS Qty,
                         CASE WHEN t1.order_weightvolume > t1.order_weightactual THEN
                             t1.order_weightvolume
                         ELSE
                             t1.order_weightactual
                         END
                            AS Weight,
                         t1.order_status AS StatusId,
                         (SELECT t2.mst_codedesc
                            FROM trucker_mstcodedetail t2
                           WHERE t2.mst_codegroup = 'stodr'
                             AND t2.mst_codedetail = t1.order_status) AS StatusName
                    FROM shipper_orderdetail t1
                   WHERE t1.order_assigndriverid = '".$p_param['driverid']."'
                     AND t1.order_status != 6
                     AND EXISTS (SELECT *
                                   FROM trucker_driver t2
                                  WHERE t1.order_assigndriverid = t2.id_driver
                                    AND token = '".$p_param['token']."') ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['PoliceNumber'])
        {
            $data['OrderId'] = $row['OrderId'];
            $data['OrderDetailId'] = $row['OrderDetailId'];
            $data['PONomber'] = $row['PONomber'];
            $data['ShipmentNumber'] = $row['ShipmentNumber'];
            $data['PoliceNumber'] = $row['PoliceNumber'];
            $data['ShipperName'] = $row['ShipperName'];
            $data['OriginName'] = $row['OriginName'];
            $data['OriginAddress'] = $row['OriginAddress'];
            $data['OriginCoordinate'] = $row['OriginCoordinate'];
            $data['OriginPICName'] = $row['OriginPICName'];
            $data['OriginPICPhone'] = $row['OriginPICPhone'];
            $data['DestinationName'] = $row['DestinationName'];
            $data['DestinationAddress'] = $row['DestinationAddress'];
            $data['DestinationCoordinate'] = $row['DestinationCoordinate'];
            $data['DestinationPICName'] = $row['DestinationPICName'];
            $data['DestinationPICPhone'] = $row['DestinationPICPhone'];
            $data['Qty'] = $row['Qty'];
            $data['Weight'] = $row['Weight'];
            $data['StatusId'] = $row['StatusId'];
            $data['StatusName'] = $row['StatusName'];
        }

        if(empty($data['OrderId']))
        {
            $status = false;
        }
        else{
            $status = true;
        }

        $result = array('status' => $status, 'order' => $data);

        return $result;
    }

    public function newupdate_token($p_token, $p_token_activity, $p_username)
    {
        $query = "UPDATE trucker_driver
                     SET token = '".$p_token_activity."'
                   WHERE id_driver = '".$p_username."'
                     AND token = '".$p_token."'";

        $success = $this->db->execute($query);
        
        if($success > 0)
        {
            $results = true;
        }
        else
        {
            $results = false;
        }
        return $results;
    }

    public function newupdate_status($p_param)
    {
        $query = "UPDATE trucker_asignorder t1
                     SET t1.assign_driverstatus = '".$p_param['status']."'
                   WHERE EXISTS (SELECT *
                                   FROM trucker_driver t2
                                  WHERE (username = '".$p_param['username']."' OR email = '".$p_param['username']."' OR no_telp = '".$p_param['username']."')
                                    AND t1.assign_driverid = t2.id_driver )";

        $success = $this->db->execute($query);
        
        if($success > 0)
        {
            $update_token = $this->newupdate_token($p_param['token'], $p_param['activity_token'], $p_param['username']);
            $results = $update_token;
        }
        else
        {
            $results = false;
        }
        return $results;
    }

}
