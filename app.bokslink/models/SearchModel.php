<?php


/**
 *
 */
class SearchModel extends BaseModel
{

  public function getLocation($param)
  {
    $query = "SELECT id,boks_cluster_id, name, address, post_code,
                kel_code, latitude, longitude,
                boks_point_info, is_boks_point_orig,
                kec_code, kokab_code, prov_code,
                address_1, address_2,
                is_boks_point_dest,
                is_boks_point_able
                FROM boks_building
                WHERE name LIKE '%".$param."%'
                LIMIT 6";

    $result = $this->db->query($query);
    $row = $result->fetchArray();
    $i=0;

    if($row > 0){
      while($row = $result->fetchArray()){
        $data[$i]['no'] = $i + 1;
        $data[$i]['lokasi_id'] = $row['id'];
        $data[$i]['lokasi_boks_cluster_id'] = $row['boks_cluster_id'];
        $data[$i]['lokasi_name'] = $row['name'];
        $data[$i]['lokasi_address'] = $row['address'];
        $data[$i]['lokasi_post_code'] = $row['post_code'];
        $data[$i]['lokasi_kel_code'] = $row['kel_code'];
        $data[$i]['lokasi_latitude'] = $row['latitude'];
        $data[$i]['lokasi_longitude'] = $row['longitude'];
        $data[$i]['lokasi_boks_point_info'] = $row['boks_point_info'];
        $data[$i]['lokasi_is_boks_point_orig'] = $row['is_boks_point_orig'];
        $data[$i]['lokasi_kec_code'] = $row['kec_code'];
        $data[$i]['lokasi_kokab_code'] = $row['kokab_code'];
        $data[$i]['lokasi_prov_code'] = $row['prov_code'];
        $data[$i]['lokasi_address_1'] = $row['address_1'];
        $data[$i]['lokasi_address_2'] = $row['address_2'];
        $data[$i]['lokasi_is_boks_point_dest'] = $row['is_boks_point_dest'];
        $data[$i]['lokasi_is_boks_point_able'] = $row['is_boks_point_able'];
        $i = $i+1;
      }
      $status = true;
    }


    else{
      $data['no'] = '';
      $data['lokasi_id'] = '';
      $data['lokasi_boks_cluster_id'] = '';
      $data['lokasi_name'] = '';
      $data['lokasi_address'] = '';
      $data['lokasi_post_code'] = '';
      $data['lokasi_kel_code'] = '';
      $data['lokasi_latitude'] = '';
      $data['lokasi_longitude'] = '';
      $data['lokasi_boks_point_info'] = '';
      $data['lokasi_is_boks_point_orig'] = '';
      $data['lokasi_kec_code'] = '';
      $data['lokasi_kokab_code'] = '';
      $data['lokasi_prov_code'] = '';
      $data['lokasi_address_1'] = '';
      $data['lokasi_address_2'] = '';
      $data['lokasi_is_boks_point_dest'] = '';
      $data['lokasi_is_boks_point_able'] = '';
      $status = false;
    }

    $dataLocation = $data;
    $results = array('status' => $status, 'data'=>$dataLocation);
    return $results;
  }

}

 ?>
