<?php

class Master extends BaseModel
{
    public function get_address($p_param)
    {
        $query = "SELECT id, CONCAT(name, ' ', address) AS alamat
                    FROM boks_building
                   WHERE (name LIKE '%".$p_param['address']."%' OR address LIKE '%".$p_param['address']."%') ";

        $result = $this->db->query($query);

        $i = 0;

        if($result)
        {
            while($row=$result->fetchArray())
            {
                $data[$i]['no'] = $i+1;
                $data[$i]['loc_id'] = $row['id'];
                $data[$i]['loc_address'] = $row['alamat'];
                $i = $i+1;
            }
            $status = 'true';
        }
        else
        {
            $data[0]['no'] = '';
            $data[0]['loc_id'] = '';
            $data[0]['loc_address'] = '';
            $status = 'false';
        }


        $address_location = $data;


        $result = array('status' => $status, 'pickup_location' => $address_location);

        return $address_location;
    }

    public function get_locationlist($p_param)
    {
        $query = "SELECT id AS Loc_Id,
                         label AS Loc_Name,
                         CONCAT(address1, ', ', address2, ', ', address3) As Loc_Address,
                         kel_code AS Loc_PostalCode
                    FROM shipper_location
                   WHERE shipper_id = '".$p_param['shipperid']."'
                     AND type = '".$p_param['type']."' ";

        $result = $this->db->query($query);

        $i = 0;

        if($result)
        {
            while($row=$result->fetchArray())
            {
                $data[$i]['no'] = $i+1;
                $data[$i]['Loc_Id'] = $row['Loc_Id'];
                $data[$i]['Loc_Name'] = $row['Loc_Name'];
                $data[$i]['Loc_Address'] = $row['Loc_Address'];
                $data[$i]['Loc_PostalCode'] = $row['Loc_PostalCode'];
                $i = $i+1;
            }
            $status = 'true';
        }
        else
        {
            $data[0]['no'] = '';
            $data[$i]['Loc_Id'] = '';
            $data[$i]['Loc_Name'] = '';
            $data[$i]['Loc_Address'] = '';
            $data[$i]['Loc_PostalCode'] = '';
            $status = 'false';
        }


        $address_location = $data;


        $result = array('status' => $status, 'address_location' => $address_location);

        return $result;
    }








    public function update_token($p_token, $p_username)
    {
        $query = "UPDATE shipper
                     SET token = '".$p_token."'
                   WHERE (email_address = '".$p_username."' OR mobile_number = '".$p_username."')";

        $success = $this->db->execute($query);

        if($success > 0)
        {
            $results = true;
        }
        else
        {
            $results = false;
        }
        return $results;
    }

    public function get_auth($p_param)
    {
        $query = "SELECT id,
                         email_address,
                         mobile_number,
                         CONCAT(first_name, ' ', last_name) AS full_name,
                         CASE WHEN gender = 1 THEN
                            'Male'
                         ElSE
                            'Female'
                         END
                            AS gender,
                         occupation,
                         d_o_b AS dateofbirth,
                         status,
                         is_login
                    FROM shipper
                   WHERE (email_address = '".$p_param['username']."' OR mobile_number = '".$p_param['username']."')
                     AND password = '".$p_param['password']."' ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['id'])
        {
            $data['shipper_id'] = $row['id'];
            $data['shipper_email'] = $row['email_address'];
            $data['shipper_fullname'] = $row['full_name'];
            $data['shipper_gender'] = $row['gender'];
            $data['shipper_occupation'] = $row['occupation'];
            $data['shipper_dateofbirth'] = $row['dateofbirth'];
            $data['shipper_status'] = $row['status'];
            $data['shipper_islogin'] = $row['is_login'];
            $status = 'true';
        }
        else{
            $data['shipper_id'] = '';
            $data['shipper_email'] = '';
            $data['shipper_fullname'] = '';
            $data['shipper_gender'] = '';
            $data['shipper_occupation'] = '';
            $data['shipper_dateofbirth'] = '';
            $data['shipper_status'] = '';
            $data['shipper_islogin'] = '';
            $status = 'false';
        }

        $profile = $data;


        $result = array('status' => $status, 'profile' => $profile);

        return $result;
    }

    public function get_checkRegistered($p_param)
    {
        $query = "SELECT COUNT(*) AS data_shipper
                    FROM shipper
                   WHERE (email_address = '".$p_param['email']."' OR mobile_number = '".$p_param['email']."') ";

        $result = $this->db->query($query);
        $row = $result->fetchArray();

        //print_r($row['data_shipper']);die;
        if($row['data_shipper'] == 1)
        {
            $data['statusReg'] = 'Registered';
            $status = false;
        }
        else{
            $query = "INSERT INTO shipper
                            (
                                email_address, mobile_number, first_name, last_name, gender,
                                occupation, password, d_o_b, status, fb_userid, ggl_userid,
                                newsletter_status, created_time, updated_time, ref_activation,
                                is_login, token, token_firebase, ref_activation_exp, reset_pwd_type
                            )
                    VALUES
                            (
                                '".$p_param['email']."',
                                '+628100000000',
                                '".$p_param['firstname']."',
                                '".$p_param['lastname']."',
                                '0',
                                'Karyawan',
                                '".$p_param['password']."',
                                '".$p_param['sysdate']."',
                                'New',
                                ' ',
                                ' ',
                                '0',
                                '".$p_param['sysdate']."',
                                '".$p_param['sysdate']."',
                                ' ',
                                '0',
                                '".$p_param['activity_token']."',
                                ' ',
                                '".$p_param['sysdate']."',
                                '0'
                            )";

            $success = $this->db->execute($query);

            if($success > 0)
            {
                $status = true;
                $data['statusReg'] = 'New Register';
            }
            else
            {
                $status = false;
                $data['statusReg'] = '';
            }
        }

        $StatusReg = $data;


        $result = array('status' => $status, 'StatusReg' => $StatusReg);

        return $result;
    }

    public function register($p_param)
    {
        $query = "INSERT INTO shipper
                         (
                            email_address, mobile_number, first_name, last_name, gender,
                            occupation, password, d_o_b, status, fb_userid, ggl_userid,
                            newsletter_status, created_time, updated_time, ref_activation,
                            is_login, token, token_firebase, ref_activation_exp, reset_pwd_type
                         )
                   VALUES
                         (
                            '".$p_param['email']."',
                            '+628100000000',
                            '".$p_param['firstname']."',
                            '".$p_param['lastname']."',
                            '0',
                            'Karyawan',
                            '".$p_param['password']."',
                            '".$p_param['sysdate']."',
                            'New',
                            ' ',
                            ' ',
                            '0',
                            '".$p_param['sysdate']."',
                            '".$p_param['sysdate']."',
                            ' ',
                            '0',
                            '".$p_param['activity_token']."',
                            ' ',
                            '".$p_param['sysdate']."',
                            '0'
                         )";

        $success = $this->db->execute($query);

        if($success > 0)
        {
            $status = true;
        }
        else
        {
            $status = false;
        }

        $result = array('status' => $status);

        return $result;
    }








    public function get_authx($p_param)
    {
        $query = "SELECT t1.id_driver AS driver_id,
                         t1.name AS driver_name,
                         CONCAT('".$p_param['driver_image']."', t1.image_driver) AS driver_image,
                         '".$p_param['upload_image']."' AS upload_image,
                         t1.id_trucker AS trucker_id,
                         (SELECT t2.company_name FROM trucker t2 WHERE t1.id_trucker = t2.id_trucker) AS trucker_name
                    FROM shipper t1
                   WHERE (t1.username = '".$p_param['username']."' OR t1.email = '".$p_param['username']."' OR t1.no_telp = '".$p_param['username']."')
                     AND t1.password = '".$p_param['password']."' ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['driver_id'])
        {
            $data['driver_id'] = $row['driver_id'];
            $data['driver_name'] = $row['driver_name'];
            $data['driver_image'] = $row['driver_image'];
            $data['url_upload'] = $row['upload_image'];
            $data['trucker_id'] = $row['trucker_id'];
            $data['trucker_name'] = $row['trucker_name'];
        }
        else{
            $data['driver_id'] = '';
            $data['driver_name'] = '';
            $data['driver_image'] = '';
            $data['url_upload'] = '';
            $data['trucker_id'] = '';
            $data['trucker_name'] = '';
        }

        $profile = $data;

        if(!empty($profile['driver_name']))
        {
            $order = $this->get_order($data['driver_id']);
            $status = true;
        }
        else {
            $order = $this->get_order($data['driver_id']);
            $status = false;
        }

        $result = array('status' => $status, 'profile' => $profile, 'order' => $order);

        return $result;
    }

    public function get_order($p_driverid)
    {
        $query = "SELECT t1.order_id AS OrderId,
                         t1.order_detailid AS OrderDetailId,
                         (SELECT t2.order_ponumber FROM shipper_order t2 WHERE t1.order_id = t2.order_id) AS PONomber,
                         t1.order_shipmentnumber AS ShipmentNumber,
                         (SELECT t2.no_polisi
                            FROM trucker_fleet t2
                           WHERE t1.order_assigntruckfleetid = t2.id_truck_fleet)
                            AS PoliceNumber,
                         'PT. INDOFOOD INDONESIA' AS ShipperName,
                         'GUDANG MODENA INDONESIA' AS OriginName,
                         t1.order_originaddress AS OriginAddress,
                         t1.order_origincoordinate AS OriginCoordinate,
                         'Edy SW' AS OriginPICName,
                         '62811900091' AS OriginPICPhone,
                         t1.order_destinationname AS DestinationName,
                         t1.order_destinationaddress AS DestinationAddress,
                         t1.order_destinationcoordinate AS DestinationCoordinate,
                         'Meyyer' AS DestinationPICName,
                         '6281190034564' AS DestinationPICPhone,
                         t1.order_quantity AS Qty,
                         CASE WHEN t1.order_weightvolume > t1.order_weightactual THEN
                             t1.order_weightvolume
                         ELSE
                             t1.order_weightactual
                         END
                            AS Weight,
                         t1.order_status AS StatusId,
                         (SELECT t2.mst_codedesc
                            FROM trucker_mstcodedetail t2
                           WHERE t2.mst_codegroup = 'stodr'
                             AND t2.mst_codedetail = t1.order_status) AS StatusName
                    FROM shipper_orderdetail t1
                   WHERE t1.order_assigndriverid = '".$p_driverid."'
                     AND t1.order_status != 7 ";

        $result = $this->db->query($query);

        $row = $result->fetchArray();

        if($row['PoliceNumber'])
        {
            $data['OrderId'] = $row['OrderId'];
            $data['OrderDetailId'] = $row['OrderDetailId'];
            $data['PONomber'] = $row['PONomber'];
            $data['ShipmentNumber'] = $row['ShipmentNumber'];
            $data['PoliceNumber'] = $row['PoliceNumber'];
            $data['ShipperName'] = $row['ShipperName'];
            $data['OriginName'] = $row['OriginName'];
            $data['OriginAddress'] = $row['OriginAddress'];
            $data['OriginCoordinate'] = $row['OriginCoordinate'];
            $data['OriginPICName'] = $row['OriginPICName'];
            $data['OriginPICPhone'] = $row['OriginPICPhone'];
            $data['DestinationName'] = $row['DestinationName'];
            $data['DestinationAddress'] = $row['DestinationAddress'];
            $data['DestinationCoordinate'] = $row['DestinationCoordinate'];
            $data['DestinationPICName'] = $row['DestinationPICName'];
            $data['DestinationPICPhone'] = $row['DestinationPICPhone'];
            $data['Qty'] = $row['Qty'];
            $data['Weight'] = $row['Weight'];
            $data['StatusId'] = $row['StatusId'];
            $data['StatusName'] = $row['StatusName'];
            $data['statusorder'] = true;
        }
        else{
            $data['OrderId'] = '';
            $data['OrderDetailId'] = '';
            $data['PONomber'] = '';
            $data['ShipmentNumber'] = '';
            $data['PoliceNumber'] = '';
            $data['ShipperName'] = '';
            $data['OriginName'] = '';
            $data['OriginAddress'] = '';
            $data['OriginCoordinate'] = '';
            $data['OriginPICName'] = '';
            $data['OriginPICPhone'] = '';
            $data['DestinationName'] = '';
            $data['DestinationAddress'] = '';
            $data['DestinationCoordinate'] = '';
            $data['DestinationPICName'] = '';
            $data['DestinationPICPhone'] = '';
            $data['Qty'] = '';
            $data['Weight'] = '';
            $data['StatusId'] = '';
            $data['StatusName'] = '';
            $data['statusorder'] = false;
        }

        return $data;
    }

    public function get_profileaddress($p_param)
    {
        $query = "SELECT *
                    FROM mdm_usersaddress
                   WHERE user_id = '".$p_param."'";

        $result = $this->db->query($query);

        $i = 0;
        $data = array();
        while($row=$result->fetchArray())
        {
            $data[$i]['user_id'] = $row['user_id'];
            $data[$i]['user_name'] = $row['user_name'];
            $data[$i]['user_locationid'] = $row['user_locationid'];
            $data[$i]['user_locationname'] = $row['user_locationname'];
            $data[$i]['user_pinpoint'] = $row['user_pinpoint'];
            $data[$i]['user_address'] = $row['user_address'];
            $data[$i]['user_citydistrict'] = $row['user_citydistrict'];
            $data[$i]['activestatus'] = $row['activestatus'];
            $data[$i]['created_at'] = $row['created_at'];
            $data[$i]['created_by'] = $row['created_by'];
            $data[$i]['updated_at'] = $row['updated_at'];
            $data[$i]['updated_by'] = $row['updated_by'];
            $data[$i]['deleted_at'] = $row['deleted_at'];

            $i = $i+1;
        }

        return $data;
    }

}
