<?php

/**
 *
 */
class OrderInBokspointModel extends BaseModel
{

  public function getBokspointOrder($param)
  {
    $query = "SELECT order_code,
    partner_id,
    pickup_contact_name,
    pickup_address2,
    order_status,
    service_type,
    subservice_type,
    pickup_contact_mb,
    drop_contact_name,
    drop_address1,
    drop_contact_mb,
    goods_category,
    goods_subcategory,
    goods_quantity,
    goods_value,
    goods_dimension_length,
    goods_dimension_width,
    goods_dimension_height,
    goods_dimension_equiweight,
    goods_dimension_weight,
    complete_photo,
    address_1,
    address_2
    FROM shipper_order a join boks_building b ON a.building_pickup_id = b.id
    join boks_partner c ON a.building_pickup_id = c.boks_building_id
    WHERE b.boks_cluster_id = '".$param['boks_cluster_id']."' and DATE(a.created_time) = curdate()+0
    ";


    $success = $this->db->query($query);
    $i=0;

    if($success){
      while ($row = $success->fetchArray()) {
        // code...
        $data[$i]['no'] = $i+1;
        $data[$i]['order_code'] = $row['order_code'];
        $data[$i]['partner_id'] = $row['partner_id'];
        $data[$i]['pickup_contact_name'] = $row['pickup_contact_name'];
        $data[$i]['pickup_address2'] = $row['pickup_address2'];
        $data[$i]['order_status'] = $row['order_status'];
        $data[$i]['service_type'] = $row['service_type'];
        $data[$i]['subservice_type'] = $row['subservice_type'];
        $data[$i]['pickup_contact_mb'] = $row['pickup_contact_mb'];
        $data[$i]['drop_contact_name'] = $row['drop_contact_name'];
        $data[$i]['drop_address1'] = $row['drop_address1'];
        $data[$i]['drop_contact_mb'] = $row['drop_contact_mb'];
        $data[$i]['goods_category'] = $row['goods_category'];
        $data[$i]['goods_subcategory'] = $row['goods_subcategory'];
        $data[$i]['goods_quantity'] = $row['goods_quantity'];
        $data[$i]['goods_value'] = $row['goods_value'];
        $data[$i]['goods_dimension_length'] = $row['goods_dimension_length'];
        $data[$i]['goods_dimension_width'] = $row['goods_dimension_width'];
        $data[$i]['goods_dimension_height'] = $row['goods_dimension_height'];
        $data[$i]['goods_dimension_equiweight'] = $row['goods_dimension_equiweight'];
        $data[$i]['goods_dimension_weight'] = $row['goods_dimension_weight'];
        $data[$i]['complete_photo'] = $row['complete_photo'];
        $data[$i]['address_1'] = $row['address_1'];
        $data[$i]['address_2'] = $row['address_2'];
        $i = $i+1;
      }
      $status = true;
    }
    else {
      // code...
      $data[$i]['no']='';
    }
    $databokspoint = $data;
    $result = array('status' => $status, 'data' => $databokspoint);
    return $result;
  }


  public function getBokspointOrder_2($param){
    $query = " SELECT order_id, order_code
                FROM shipper_order
                WHERE building_pickup_id = ( SELECT id
                                              FROM boks_building
                                              WHERE boks_cluster_id = '".$param['boks_cluster_id']."'
                ) and DATE(shipper_order.created_time) = curdate()+0";
    $success = $this->db->query($query);
    $row = $success->fetchArray();
      if($row['order_id'])
      {
        $data['order_id'] = $row['order_id'];
        $data['order_code'] = $row['order_code'];
        $status = 'true';
      }
      else{
        $data['order_id'] = '';
        $data['order_code'] = '';
        $status = 'false';
      }

        if($status == 'true'){
          $building = $data;
        }
        else{
          $building = $data;
        }
      return $building;
  }


}

 ?>
