<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('ID');
            $table->String('Name');
            $table->String('Address');
            $table->String('Username')->unique();
            $table->String('Email');
            $table->String('Password');
            $table->String('Type');
            // ID, Name, Address, Username, Email, Password, Type, Created_at, Updated_at, Deleted_at,Created_at,Updated_at,Deleted_by
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
